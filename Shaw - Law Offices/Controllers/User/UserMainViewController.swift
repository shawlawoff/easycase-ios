//
//  UserMainViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import LGButton
import PopupDialog
import MBRadioCheckboxButton
import UICircularProgressRing
import FirebaseAuth
import FirebaseDatabase
import FirebaseInstanceID
import MessageUI
import KRProgressHUD

class UserMainViewController: BaseViewController {
    
    @IBOutlet weak var notifyBadge: LGButton!
    @IBOutlet weak var viewReminderContainer: UIView!
    @IBOutlet weak var lblReminderMsg: UILabel!
    
    var currentReminder: NotificationModel? = nil
    
    @IBOutlet weak var progressRing: UICircularProgressRing!
    @IBOutlet weak var btnEstimatedValue: UIButton!
    
    @IBOutlet weak var btnCaseSteps: LGButton!
    @IBOutlet weak var containerCaseSteps: UIStackView!
    var checkFeeds = [Any]()
    var caseStepsType: String? = nil
    
    @IBOutlet weak var btnValueHistory: LGButton!
    @IBOutlet weak var btnUpcoming: LGButton!
    @IBOutlet weak var btnMedical: LGButton!
    @IBOutlet weak var btnPain: LGButton!
    @IBOutlet weak var btnTalkNow: LGButton!
    
    @IBOutlet weak var containerValueHistory: UIStackView!
    @IBOutlet weak var containerUpcomingDates: UIStackView!
    @IBOutlet weak var containerDocketView: UIView!
    @IBOutlet weak var containerMedicalRecords: UIStackView!
    @IBOutlet weak var textPainView: UITextView!
    @IBOutlet weak var containerTalkNow: UIStackView!
    
    let uid = Auth.auth().currentUser!.uid
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        progressRing.maxValue = 100
        progressRing.value = 0
        progressRing.style = .ontop
        progressRing.valueKnobStyle = UICircularRingValueKnobStyle(size: 10, color: Color.greenRingColor)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                KDataRefUser.child(Auth.auth().currentUser!.uid).child("regToken").setValue(result.token)
            }
        }
    }
    
    override func initializeControls() {
        btnCaseSteps.alpha = 0.6
        btnValueHistory.alpha = 0.6
        btnUpcoming.alpha = 0.6
        btnMedical.alpha = 0.6
        btnPain.alpha = 0.6
        btnTalkNow.alpha = 0.6
        
        textPainView.layer.borderWidth = 1
        textPainView.layer.borderColor = UIColor.black.cgColor
        textPainView.delegate = self
        
        for index in 1...KTextCaseSteps.count {
            var height = getHeight(text: KTextCaseSteps[index-1], width: UIScreen.main.bounds.width - 30 - 20)
            if height < 25 {
                height = 25
            }
            let newCheckBtn = CheckboxButton(frame: CGRect(x: 0, y: 0, width: 100, height: height))
            newCheckBtn.isUserInteractionEnabled = false
            newCheckBtn.setTitle(KTextCaseSteps[index-1], for: .normal)
            newCheckBtn.setTitleColor(UIColor.black, for: .normal)
            newCheckBtn.titleLabel?.lineBreakMode = .byWordWrapping
            newCheckBtn.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 15)
            
            newCheckBtn.contentVerticalAlignment = .center
            
            newCheckBtn.translatesAutoresizingMaskIntoConstraints = false
            newCheckBtn.heightAnchor.constraint(equalToConstant: height).isActive = true
            
            containerCaseSteps.addArrangedSubview(newCheckBtn)
            checkFeeds.append(newCheckBtn)
        }
        loadData()
    }
    
    private func loadCaseSteps() {
        containerCaseSteps.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        checkFeeds.removeAll()
        if self.caseStepsType == "claim" {
            for index in 0...KTextCaseSteps.count-1 {
                let newCheckBtn = createCheckboxStep(KTextCaseSteps[index])
                newCheckBtn.isEnabled = false
                
                containerCaseSteps.addArrangedSubview(newCheckBtn)
                checkFeeds.append(newCheckBtn)
            }
            
            KDataRefClaim.child(uid).observe(.value) { (snapshot) in
                KRProgressHUD.dismiss()
                if let value = snapshot.value as? String,
                    value.count > 0 {
                    var checked = value.components(separatedBy: ",")
                    
                    for index in 1...checked.count {
                        if let pos = Int(checked[index-1]),
                            let checkButton = self.checkFeeds[pos] as? CheckboxButton {
                            checkButton.isOn = true
                        }
                    }
                    self.progressRing.value = CGFloat(checked.count * 100 / 47)
                }
            }
        } else {
            for category in 0...KTextLawSuiteTitles.count-1 {
                let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
                titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .bold)
                titleLabel.textColor = UIColor.black
                titleLabel.text = KTextLawSuiteTitles[category]
                titleLabel.translatesAutoresizingMaskIntoConstraints = false
                titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
                containerCaseSteps.addArrangedSubview(titleLabel)
                
                for step in 0...KTextLawSuite[category].count-1 {
                    
                    if KTextLawSuite[category][step].type == .checkbox {
                        let newCheckBtn = createCheckboxStep(KTextLawSuite[category][step].text)
                        newCheckBtn.isEnabled = false
                        
                        containerCaseSteps.addArrangedSubview(newCheckBtn)
                        checkFeeds.append(newCheckBtn)
                    } else if KTextLawSuite[category][step].type == .binary {
                        let singleView = createSingleChoiceView(KTextLawSuite[category][step].text)
                        singleView.chkYes.isEnabled = false
                        singleView.chkNo.isEnabled = false
                        
                        containerCaseSteps.addArrangedSubview(singleView)
                        checkFeeds.append(singleView)
                    } else if KTextLawSuite[category][step].type == .text {
                        let txtField = createTextField(KTextLawSuite[category][step].text)
                        txtField.txtContent.isEnabled = false
                        
                        containerCaseSteps.addArrangedSubview(txtField)
                        checkFeeds.append(txtField)
                    } else if KTextLawSuite[category][step].type == .textdate {
                        let dateView = createDateView(KTextLawSuite[category][step].text)
                        dateView.txtContent.isEnabled = false
                        
                        containerCaseSteps.addArrangedSubview(dateView)
                        checkFeeds.append(dateView)
                    }
                    
                }
            }
            KDataRefLawsuit.child(uid).observeSingleEvent(of: .value) { (snapshot) in
                KRProgressHUD.dismiss()
                if let value = snapshot.value as? String,
                    value.count > 0 {
                    var checked = value.components(separatedBy: ",")
                    var selectedCount = 0
                    for index in 0...checked.count-1 {
                        if let checkButton = self.checkFeeds[index] as? CheckboxButton {
                            if Int(checked[index]) == 1 {
                                checkButton.isOn = true
                                selectedCount += 1
                            } else {
                                checkButton.isOn = false
                            }
                        } else if let txtField = self.checkFeeds[index] as? CaseStepTextField {
                            txtField.content = checked[index].replacingOccurrences(of: "s1:s1", with: ",")
                            if checked[index].count > 0 {
                                selectedCount += 1
                            }
                        } else if let txtField = self.checkFeeds[index] as? CaseStepDateView {
                            txtField.content = checked[index].replacingOccurrences(of: "s1:s1", with: ",")
                            if checked[index].count > 0 {
                                selectedCount += 1
                            }
                        } else if let singleView = self.checkFeeds[index] as? SingleChoiceView {
                            singleView.selectedState = self.convertStringToQuestion(checked[index])
                            if singleView.selectedState == .yes || singleView.selectedState == .no {
                                selectedCount += 1
                            }
                        }
                    }
                    self.progressRing.value = CGFloat(selectedCount * 100 / checked.count)
                }
            }
        }
    }
    
    private func loadData() {
        KRProgressHUD.show()
        
        KDataRefNotify.child(uid).queryOrderedByKey().observe(.value) { (snapshot) in
            self.viewReminderContainer.isHidden = true
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = NotificationModel(snapshot: snapshot) {
                    if !item.isRead {
                        self.viewReminderContainer.isHidden = false
                        self.lblReminderMsg.text = item.message
                        
                        self.currentReminder = item
                        break
                    }
                }
            }
        }
        
        KDataRefCaseType.child(uid).observe(.value) { (snapshot) in
            if let value = snapshot.value as? String {
                self.caseStepsType = value
            } else {
                self.caseStepsType = "claim"
            }
            self.loadCaseSteps()
        }
        
        KDataRefValue.child(uid).observe(.value) { (snapshot) in
            if let item = CaseValueModel(snapshot: snapshot) {
                let curValue = Int64(item.price) ?? 0;
                if (curValue >= 0) {
                    self.btnEstimatedValue.setTitle("$" + String(curValue), for: .normal)
                } else {
                    self.btnEstimatedValue.setTitle("-$" + String(abs(curValue)), for: .normal)
                }
                self.textPainView.text = item.note
            }
        }
        
        KDataRefHistory.child(uid).observe(.value) { (snapshot) in
            self.containerValueHistory.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = HistoryModel(snapshot: snapshot) {
                    
                    let historyView = ValueHistoryView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    historyView.translatesAutoresizingMaskIntoConstraints = false
                    historyView.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    if (item.increase) {
                        historyView.imgMark.image = UIImage(named: "up_arrow")
                    } else {
                        historyView.imgMark.image = UIImage(named: "down_arrow")
                    }
                    historyView.lblContent.text = item.detail
                    historyView.btnDelete.isHidden = true
                    self.containerValueHistory.addArrangedSubview(historyView)
                }
            }
        }
        
        KDataRefUpcoming.child(uid).observe(.value) { (snapshot) in
            self.containerUpcomingDates.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = UpcomingDateModel(snapshot: snapshot) {
                    
                    let upcomingView = UpcomingDateView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    upcomingView.lblContent.text = "- " + item.date + " " + item.note
                    upcomingView.btnDelete.isHidden = true
                    self.containerUpcomingDates.addArrangedSubview(upcomingView)
                }
            }
        }
        
        KDataRefMedical.child(uid).observe(.value) { (snapshot) in
            self.containerMedicalRecords.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = MedicalRecordModel(snapshot: snapshot) {
                    
                    let recordView = MedicalRecordView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    recordView.setupDetail(item)
                    recordView.delegate = self
                    
                    self.containerMedicalRecords.addArrangedSubview(recordView)
                }
            }
        }
    }
    
    // MARK: - User Interaction
    
    @IBAction func reminderOkayAction(_ sender: Any) {
        if currentReminder != nil, let key=currentReminder?.ref?.key {
            KDataRefNotify.child(uid).child(key).child("isRead").setValue(true)
            currentReminder = nil
        }
    }
    
    @IBAction func caseStepsAction(_ sender: Any) {
        if (containerCaseSteps.isHidden) {
            closeAllViews()
            containerCaseSteps.isHidden = false
            btnCaseSteps.alpha = 1
        } else {
            containerCaseSteps.isHidden = true
            btnCaseSteps.alpha = 0.6
        }
    }
    
    @IBAction func valueHistoryAction(_ sender: Any) {
        if (containerValueHistory.isHidden) {
            closeAllViews()
            containerValueHistory.isHidden = false
            btnValueHistory.alpha = 1
        } else {
            containerValueHistory.isHidden = true
            btnValueHistory.alpha = 0.6
        }
    }
    
    @IBAction func upcomingDatesAction(_ sender: Any) {
        if (containerUpcomingDates.isHidden) {
            closeAllViews()
            containerUpcomingDates.isHidden = false
            containerDocketView.isHidden = false
            btnUpcoming.alpha = 1
        } else {
            containerUpcomingDates.isHidden = true
            containerDocketView.isHidden = true
            btnUpcoming.alpha = 0.6
        }
    }
    
    @IBAction func courtDocketAction(_ sender: Any) {
        guard let url = URL(string: "https://public.courts.in.gov/mycase/#/vw/Search") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func medicalRecordsAction(_ sender: Any) {
        if (containerMedicalRecords.isHidden) {
            closeAllViews()
            containerMedicalRecords.isHidden = false
            btnMedical.alpha = 1
        } else {
            containerMedicalRecords.isHidden = true
            btnMedical.alpha = 0.6
        }
    }
    
    @IBAction func painJournalAction(_ sender: Any) {
        if (textPainView.isHidden) {
            closeAllViews()
            textPainView.isHidden = false
            btnPain.alpha = 1
        } else {
            textPainView.isHidden = true
            btnPain.alpha = 0.6
        }
    }
    
    @IBAction func talkNowAction(_ sender: Any) {
        if (containerTalkNow.isHidden) {
            closeAllViews()
            containerTalkNow.isHidden = false
            btnTalkNow.alpha = 1
        } else {
            containerTalkNow.isHidden = true
            btnTalkNow.alpha = 0.6
        }
    }
    
    @IBAction func callShawAction(_ sender: Any) {
        if let url = URL(string: "tel://8772255742") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func emailShawAction(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["Jeffshaw@slipandfalls.com"])
            
            present(mail, animated: true)
        } else {
            showAlert(withTitle: "Warning", message: "Please run this app in real device")
        }
    }
    
    @IBAction func appChatAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let chatContainerVC = storyboard.instantiateViewController(withIdentifier: "ChatContainerViewController") as! ChatContainerViewController
        chatContainerVC.otherName = "Admin"
        
        self.navigationController?.pushViewController(chatContainerVC, animated: true)
    }
    
    func closeAllViews() {
        btnCaseSteps.alpha = 0.6
        btnValueHistory.alpha = 0.6
        btnUpcoming.alpha = 0.6
        btnMedical.alpha = 0.6
        btnTalkNow.alpha = 0.6
        btnPain.alpha = 0.6
        
        containerCaseSteps.isHidden = true
        containerValueHistory.isHidden = true
        containerUpcomingDates.isHidden = true
        containerDocketView.isHidden = true
        containerMedicalRecords.isHidden = true
        containerTalkNow.isHidden = true
        textPainView.isHidden = true
    }
}

extension UserMainViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UserMainViewController: MedicalRecordDelegate {
    func itemClicked(filepath: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let recordImageVC = storyboard.instantiateViewController(withIdentifier: "RecordsImageViewController") as! RecordsImageViewController
        recordImageVC.imagePath = filepath
        
        self.present(recordImageVC, animated: true, completion: nil)
    }
}

extension UserMainViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let strPain = textPainView.text!
        
        KDataRefValue.child(uid).child("note").setValue(strPain)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let strPain = textPainView.text!
        
        KDataRefValue.child(uid).child("note").setValue(strPain)
    }
}
