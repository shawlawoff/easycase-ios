//
//  ViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import MBRadioCheckboxButton

class TermsViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnCheckAgree: CheckboxButton!
    @IBOutlet weak var btnAgree: UIButton!
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if (AppManager.sharedInstance.getTerms()) {
//            self.performSegue(withIdentifier: Segue.goRegister, sender: self)
//        }
    }
    
    override func initializeControls() {
        
        btnCheckAgree.delegate = self
        
        scrollView.layer.borderWidth = 1
        scrollView.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func onAgree(_ sender: Any) {
        self.performSegue(withIdentifier: Segue.goRegister, sender: self)
    }
}

extension TermsViewController: CheckboxButtonDelegate {
    func chechboxButtonDidSelect(_ button: CheckboxButton) {
        
        //AppManager.sharedInstance.setTerms(agree: true)
        //self.performSegue(withIdentifier: Segue.goRegister, sender: self)
        btnAgree.isEnabled = true
    }
    
    func chechboxButtonDidDeselect(_ button: CheckboxButton) {
        btnAgree.isEnabled = false
    }
    
    
}

