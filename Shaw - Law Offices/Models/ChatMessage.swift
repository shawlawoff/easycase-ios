//
//  ChatMessage.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct ChatMessage {
    let ref: DatabaseReference?
    let id: String
    let senderId: String
    let message: String
    let createAt: Int64
    let is_read: Bool
    
    init(id: String, senderId: String, message: String, createAt: Int64, is_read: Bool) {
        self.ref = nil
        self.id = id
        self.senderId = senderId
        self.message = message
        self.createAt = createAt
        self.is_read = is_read
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let id = value["id"] as? String,
            let senderId = value["senderId"] as? String,
            let message = value["message"] as? String,
            let createAt = value["createAt"] as? Int64,
            let is_read = value["is_read"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.id = id
        self.senderId = senderId
        self.message = message
        self.createAt = createAt
        self.is_read = is_read
    }
    
    func toAnyObject() -> Any {
        return [
            "id": id,
            "senderId": senderId,
            "message": message,
            "createAt": createAt,
            "is_read": is_read
        ]
    }
}
