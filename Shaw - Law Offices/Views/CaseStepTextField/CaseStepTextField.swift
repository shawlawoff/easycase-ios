//
//  CaseStepTextField.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

@IBDesignable
class CaseStepTextField: CustomView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtContent: UITextField!
    
    @IBInspectable
    var title: String? {
        get {
            return lblTitle.text
        }
        set {
            lblTitle.text = newValue
        }
    }
    
    var content: String? {
        get {
            return txtContent.text
        }
        set {
            txtContent.text = newValue
        }
    }
    
    override func prepare() {
        super.prepare()
    }
}
