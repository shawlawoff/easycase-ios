//
//  MedicalRecordModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct MedicalRecordModel {
    let ref: DatabaseReference?
    let name: String
    let filepath: String
    
    init(name: String, filepath: String) {
        self.ref = nil
        self.name = name
        self.filepath = filepath
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let filepath = value["filepath"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.name = name
        self.filepath = filepath
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "filepath": filepath
        ]
    }
}
