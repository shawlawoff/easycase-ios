//
//  SingleChoiceView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import MBRadioCheckboxButton

enum QuestionState {
    case yes
    case no
    case unknown
}

protocol SingleChoiceViewDelegate {
    func changeState(state: QuestionState)
}

@IBDesignable
class SingleChoiceView: CustomView {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var radioContainer: RadioButtonContainerView!
    @IBOutlet weak var chkYes: RadioButton!
    @IBOutlet weak var chkNo: RadioButton!
    
    var delegate: SingleChoiceViewDelegate? = nil
    
    var selectedState: QuestionState {
        get {
            if (radioContainer.buttonContainer.selectedButton == chkYes) {
                return .yes
            } else if (radioContainer.buttonContainer.selectedButton == chkNo) {
                return .no
            }
            return .unknown
        }
        set {
            if newValue == .yes {
                chkYes.isOn = true
            } else if newValue == .no {
                chkNo.isOn = true
            } else {
                chkYes.isOn = false
                chkNo.isOn = false
            }
            
            if delegate != nil {
                delegate?.changeState(state: newValue)
            }
        }
    }
    
    @IBInspectable
    var question: String? {
        get {
            return lblQuestion.text
        }
        set {
            lblQuestion.text = newValue
        }
    }
    
    override func prepare() {
        super.prepare()
        
        chkYes.delegate = self
        chkNo.delegate = self
    }
}

extension SingleChoiceView: RadioButtonDelegate {
    func radioButtonDidSelect(_ button: RadioButton) {
        if delegate != nil {
            delegate?.changeState(state: self.selectedState)
        }
    }
    
    func radioButtonDidDeselect(_ button: RadioButton) {
        if delegate != nil {
            delegate?.changeState(state: self.selectedState)
        }
    }
}
