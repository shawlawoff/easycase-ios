//
//  AdminMainViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/9/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import LGButton
import PopupDialog
import MBRadioCheckboxButton
import UICircularProgressRing
import FirebaseDatabase
import KRProgressHUD

class AdminMainViewController: BaseViewController {

    var user: UserModel!
    var previousValue: Int64 = 0
    var caseStepsType: String? = nil
    
    @IBOutlet weak var progressRing: UICircularProgressRing!
    @IBOutlet weak var btnEstimatedValue: UIButton!
    
    @IBOutlet weak var btnCaseSteps: LGButton!
    @IBOutlet weak var containerCaseSteps: UIStackView!
    @IBOutlet weak var segmentCaseSteps: UISegmentedControl!
    var checkFeeds = [Any]()
    
    @IBOutlet weak var btnValueHistory: LGButton!
    @IBOutlet weak var btnUpcoming: LGButton!
    @IBOutlet weak var btnMedical: LGButton!
    @IBOutlet weak var btnPain: LGButton!
    
    @IBOutlet weak var containerValueHistory: UIStackView!
    @IBOutlet weak var containerAddingValueView: UIView!
    @IBOutlet weak var containerUpcomingDates: UIStackView!
    @IBOutlet weak var containerAddingUpcomingView: UIView!
    @IBOutlet weak var containerMedicalRecords: UIStackView!
    @IBOutlet weak var containerAddingMedicalView: UIView!
    @IBOutlet weak var textPainView: UITextView!
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        progressRing.maxValue = 100
        progressRing.value = 0
        progressRing.style = .ontop
        progressRing.valueKnobStyle = UICircularRingValueKnobStyle(size: 10, color: Color.greenRingColor)
    }

    override func initializeControls() {
        btnCaseSteps.alpha = 0.6
        btnValueHistory.alpha = 0.6
        btnUpcoming.alpha = 0.6
        btnMedical.alpha = 0.6
        btnPain.alpha = 0.6
        
        textPainView.layer.borderWidth = 1
        textPainView.layer.borderColor = UIColor.black.cgColor
        loadData()
    }
    
    private func loadCaseSteps() {
        containerCaseSteps.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        checkFeeds.removeAll()
        self.progressRing.value = 0
        if self.caseStepsType == "claim" {
            self.segmentCaseSteps.selectedSegmentIndex = 0
            for index in 0...KTextCaseSteps.count-1 {
                let newCheckBtn = createCheckboxStep(KTextCaseSteps[index])
                newCheckBtn.delegate = self
                
                containerCaseSteps.addArrangedSubview(newCheckBtn)
                checkFeeds.append(newCheckBtn)
            }
            
            KDataRefClaim.child(user.uid).observeSingleEvent(of: .value) { (snapshot) in
                KRProgressHUD.dismiss()
                if let value = snapshot.value as? String,
                    value.count > 0 {
                    var checked = value.components(separatedBy: ",")
                    
                    for index in 1...checked.count {
                        if let pos = Int(checked[index-1]),
                            let checkButton = self.checkFeeds[pos] as? CheckboxButton {
                            checkButton.isOn = true
                        }
                    }
                    self.progressRing.value = CGFloat(checked.count * 100 / 47)
                }
            }
        } else {
            self.segmentCaseSteps.selectedSegmentIndex = 1
            for category in 0...KTextLawSuiteTitles.count-1 {
                let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
                titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .bold)
                titleLabel.textColor = UIColor.black
                titleLabel.text = KTextLawSuiteTitles[category]
                titleLabel.translatesAutoresizingMaskIntoConstraints = false
                titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
                containerCaseSteps.addArrangedSubview(titleLabel)
                
                for step in 0...KTextLawSuite[category].count-1 {
                    
                    if KTextLawSuite[category][step].type == .checkbox {
                        let newCheckBtn = createCheckboxStep(KTextLawSuite[category][step].text)
                        newCheckBtn.delegate = self
                        
                        containerCaseSteps.addArrangedSubview(newCheckBtn)
                        checkFeeds.append(newCheckBtn)
                    } else if KTextLawSuite[category][step].type == .binary {
                        let singleView = createSingleChoiceView(KTextLawSuite[category][step].text)
                        singleView.delegate = self
                        
                        containerCaseSteps.addArrangedSubview(singleView)
                        checkFeeds.append(singleView)
                    } else if KTextLawSuite[category][step].type == .text {
                        let txtField = createTextField(KTextLawSuite[category][step].text)
                        txtField.txtContent.delegate = self
                        
                        containerCaseSteps.addArrangedSubview(txtField)
                        checkFeeds.append(txtField)
                    } else if KTextLawSuite[category][step].type == .textdate {
                        let dateView = createDateView(KTextLawSuite[category][step].text)
                        dateView.delegate = self
                        
                        containerCaseSteps.addArrangedSubview(dateView)
                        checkFeeds.append(dateView)
                    }
                    
                }
            }
            KDataRefLawsuit.child(user.uid).observeSingleEvent(of: .value) { (snapshot) in
                KRProgressHUD.dismiss()
                if let value = snapshot.value as? String,
                    value.count > 0 {
                    var checked = value.components(separatedBy: ",")
                    var selectedCount = 0
                    for index in 0...checked.count-1 {
                        if let checkButton = self.checkFeeds[index] as? CheckboxButton {
                            if Int(checked[index]) == 1 {
                                checkButton.isOn = true
                                selectedCount += 1
                            } else {
                                checkButton.isOn = false
                            }
                        } else if let txtField = self.checkFeeds[index] as? CaseStepTextField {
                            txtField.content = checked[index].replacingOccurrences(of: "s1:s1", with: ",")
                            if checked[index].count > 0 {
                                selectedCount += 1
                            }
                        } else if let txtField = self.checkFeeds[index] as? CaseStepDateView {
                            txtField.content = checked[index].replacingOccurrences(of: "s1:s1", with: ",")
                            if checked[index].count > 0 {
                                selectedCount += 1
                            }
                        } else if let singleView = self.checkFeeds[index] as? SingleChoiceView {
                            singleView.selectedState = self.convertStringToQuestion(checked[index])
                            if singleView.selectedState == .yes || singleView.selectedState == .no {
                                selectedCount += 1
                            }
                        }
                    }
                    self.progressRing.value = CGFloat(selectedCount * 100 / checked.count)
                }
            }
        }
    }
    
    private func loadData() {
        
        KRProgressHUD.show()
        KDataRefCaseType.child(user.uid).observe(.value) { (snapshot) in
            if let value = snapshot.value as? String {
                self.caseStepsType = value
            } else {
                self.caseStepsType = "claim"
            }
            self.loadCaseSteps()
        }
        
        KDataRefValue.child(user.uid).observe(.value) { (snapshot) in
            if let item = CaseValueModel(snapshot: snapshot) {
                self.previousValue = Int64(item.price) ?? 0
                if (self.previousValue >= 0) {
                    self.btnEstimatedValue.setTitle("$" + String(self.previousValue), for: .normal)
                } else {
                    self.btnEstimatedValue.setTitle("-$" + String(abs(self.previousValue)), for: .normal)
                }
                self.textPainView.text = item.note
            }
        }
        
        KDataRefHistory.child(user.uid).observe(.value) { (snapshot) in
            self.containerValueHistory.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot,
                    let item = HistoryModel(snapshot: childSnapshot) {
                    
                    let historyView = ValueHistoryView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    historyView.translatesAutoresizingMaskIntoConstraints = false
                    historyView.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    if (item.increase) {
                        historyView.imgMark.image = UIImage(named: "up_arrow")
                    } else {
                        historyView.imgMark.image = UIImage(named: "down_arrow")
                    }
                    historyView.lblContent.text = item.detail
                    historyView.childKey = childSnapshot.key
                    historyView.delegate = self
                    historyView.historyModel = item
                    
                    self.containerValueHistory.addArrangedSubview(historyView)
                }
            }
        }
        
        KDataRefUpcoming.child(user.uid).observe(.value) { (snapshot) in
            self.containerUpcomingDates.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = UpcomingDateModel(snapshot: snapshot) {
                    
                    let upcomingView = UpcomingDateView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    upcomingView.lblContent.text = "- " + item.date + " " + item.note
                    upcomingView.childKey = snapshot.key
                    upcomingView.delegate = self
                    
                    self.containerUpcomingDates.addArrangedSubview(upcomingView)
                }
            }
        }
        
        KDataRefMedical.child(user.uid).observe(.value) { (snapshot) in
            self.containerMedicalRecords.subviews.forEach({ $0.removeFromSuperview() })
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = MedicalRecordModel(snapshot: snapshot) {
                    
                    let recordView = MedicalRecordView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
                    recordView.setupDetail(item)
                    recordView.delegate = self
                    
                    self.containerMedicalRecords.addArrangedSubview(recordView)
                }
            }
        }
    }
    
    // MARK: - User Interaction
    @IBAction func backCasesAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reminderAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let reminderVC = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        reminderVC.user = user
        
        navigationController?.pushViewController(reminderVC, animated: true)
    }
    
    @IBAction func addValuePostiveAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let inputVC = storyboard.instantiateViewController(withIdentifier: "EstimatedInputViewController") as! EstimatedInputViewController
        inputVC.user = user
        inputVC.previousValue = self.previousValue
        inputVC.isPositive = true
        
        let inputVCPopup = PopupDialog(viewController: inputVC)
        self.present(inputVCPopup, animated: true, completion: nil)
    }
    
    @IBAction func addValueNegativeAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let inputVC = storyboard.instantiateViewController(withIdentifier: "EstimatedInputViewController") as! EstimatedInputViewController
        inputVC.user = user
        inputVC.previousValue = self.previousValue
        inputVC.isPositive = false
        
        let inputVCPopup = PopupDialog(viewController: inputVC)
        self.present(inputVCPopup, animated: true, completion: nil)
    }
    
    @IBAction func caseStepsAction(_ sender: Any) {
        if (containerCaseSteps.isHidden) {
            closeAllViews()
            segmentCaseSteps.isHidden = false
            containerCaseSteps.isHidden = false
            btnCaseSteps.alpha = 1
        } else {
            segmentCaseSteps.isHidden = true
            containerCaseSteps.isHidden = true
            btnCaseSteps.alpha = 0.6
        }
    }
    @IBAction func changeStepsType(_ sender: Any) {
        if (segmentCaseSteps.selectedSegmentIndex == 0) {
            KDataRefCaseType.child(user.uid).setValue("claim")
        } else {
            KDataRefCaseType.child(user.uid).setValue("lawsuite")
        }
    }
    
    @IBAction func valueHistoryAction(_ sender: Any) {
        if (containerValueHistory.isHidden) {
            closeAllViews()
            containerValueHistory.isHidden = false
            containerAddingValueView.isHidden = false
            btnValueHistory.alpha = 1
        } else {
            containerValueHistory.isHidden = true
            containerAddingValueView.isHidden = true
            btnValueHistory.alpha = 0.6
        }
    }
    
    @IBAction func upcomingDatesAction(_ sender: Any) {
        if (containerUpcomingDates.isHidden) {
            closeAllViews()
            containerUpcomingDates.isHidden = false
            containerAddingUpcomingView.isHidden = false
            btnUpcoming.alpha = 1
        } else {
            containerUpcomingDates.isHidden = true
            containerAddingUpcomingView.isHidden = true
            btnUpcoming.alpha = 0.6
        }
    }
    
    @IBAction func addUpcomingDateAction(_ sender: Any) {
        self.performSegue(withIdentifier: "openAddUpcoming", sender: self)
    }
    
    @IBAction func medicalRecordsAction(_ sender: Any) {
        if (containerMedicalRecords.isHidden) {
            closeAllViews()
            containerMedicalRecords.isHidden = false
            containerAddingMedicalView.isHidden = false
            btnMedical.alpha = 1
        } else {
            containerMedicalRecords.isHidden = true
            containerAddingMedicalView.isHidden = true
            btnMedical.alpha = 0.6
        }
    }
    
    @IBAction func addMedicalRecordAction(_ sender: Any) {
        self.performSegue(withIdentifier: "openAddMedical", sender: self)
    }
    
    @IBAction func painJournalAction(_ sender: Any) {
        if (textPainView.isHidden) {
            closeAllViews()
            textPainView.isHidden = false
            btnPain.alpha = 1
        } else {
            textPainView.isHidden = true
            btnPain.alpha = 0.6
        }
    }
    
    @IBAction func appChatAction(_ sender: Any) {
        closeAllViews()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let chatContainerVC = storyboard.instantiateViewController(withIdentifier: "ChatContainerViewController") as! ChatContainerViewController
        chatContainerVC.otherName = user.name
        chatContainerVC.otherId = user.uid
        
        self.navigationController?.pushViewController(chatContainerVC, animated: true)
    }
    
    func closeAllViews() {
        btnCaseSteps.alpha = 0.6
        btnValueHistory.alpha = 0.6
        btnUpcoming.alpha = 0.6
        btnMedical.alpha = 0.6
        btnPain.alpha = 0.6
        
        containerCaseSteps.isHidden = true
        containerValueHistory.isHidden = true
        containerAddingValueView.isHidden = true
        containerUpcomingDates.isHidden = true
        containerAddingUpcomingView.isHidden = true
        containerMedicalRecords.isHidden = true
        containerAddingMedicalView.isHidden = true
        textPainView.isHidden = true
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddUpcomingDateViewController {
            destinationVC.user = user
        } else if let destinationVC = segue.destination as? AddMedicalRecordViewController {
            destinationVC.user = user
        }
    }
    
    // MARK: - Firebase Actions
    func storeCaseSteps() {
        if self.caseStepsType == "claim" {
            var strFeed = ""
            var selectedCount = 0
            for index in 1...47 {
                if let checkButton = checkFeeds[index-1] as? CheckboxButton, checkButton.isOn {
                    selectedCount += 1
                    if (strFeed.count == 0) {
                        strFeed += String(index-1)
                    } else {
                        strFeed += ("," + String(index-1))
                    }
                }
            }
            KDataRefClaim.child(user.uid).setValue(strFeed)
            self.progressRing.value = CGFloat(selectedCount * 100 / checkFeeds.count)
        } else {
            var strFeed = "";
            var selectedCount = 0
            for index in 0...checkFeeds.count-1 {
                if let checkButton = checkFeeds[index] as? CheckboxButton {
                    if checkButton.isOn {
                        strFeed += ",1"
                        selectedCount += 1
                    } else {
                        strFeed += ",0"
                    }
                } else if let txtField = checkFeeds[index] as? CaseStepTextField {
                    strFeed += "," + txtField.content!.replacingOccurrences(of: ",", with: "s1:s1")
                    if txtField.content!.count > 0 {
                        selectedCount += 1
                    }
                } else if let txtField = checkFeeds[index] as? CaseStepDateView {
                    strFeed += "," + txtField.content!
                    if txtField.content!.count > 0 {
                        selectedCount += 1
                    }
                } else if let singleView = checkFeeds[index] as? SingleChoiceView {
                    strFeed += "," + self.convertQuestionToString(singleView.selectedState)
                    if singleView.selectedState == .yes || singleView.selectedState == .no {
                        selectedCount += 1
                    }
                }
            }
            strFeed = String(strFeed.suffix(strFeed.count-1))
            KDataRefLawsuit.child(user.uid).setValue(strFeed)
            
            self.progressRing.value = CGFloat(selectedCount * 100 / checkFeeds.count)
        }
    }
}

extension AdminMainViewController: CheckboxButtonDelegate {
    func chechboxButtonDidSelect(_ button: CheckboxButton) {
        self.storeCaseSteps()
    }
    
    func chechboxButtonDidDeselect(_ button: CheckboxButton) {
        self.storeCaseSteps()
    }
}

extension AdminMainViewController: MedicalRecordDelegate, UpcomingDateViewDelegate, ValueHistoryViewDelegate, SingleChoiceViewDelegate, CaseStepDateViewDelegate {
    func itemClicked(filepath: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let recordImageVC = storyboard.instantiateViewController(withIdentifier: "RecordsImageViewController") as! RecordsImageViewController
        recordImageVC.imagePath = filepath
        
        self.present(recordImageVC, animated: true, completion: nil)
    }
    
    func deleteUpcomingDate(_ key: String) {
        KDataRefUpcoming.child(user.uid).child(key).removeValue()
    }
    
    func delete(_ key: String, _ model: HistoryModel) {
        KDataRefHistory.child(user.uid).child(key).removeValue()
        
        if let unitPos = model.detail.lastIndex(of: "$"),
            let value = Int64(model.detail.suffix(from: model.detail.index(after: unitPos))) {
            
            let range = model.detail.index(before: unitPos)..<unitPos
            if model.detail[range] == "+" {
                KDataRefValue.child(user.uid).child("price").setValue(String(previousValue - value))
            } else {
                KDataRefValue.child(user.uid).child("price").setValue(String(previousValue + value))
            }
        }
    }
    
    func changeState(state: QuestionState) {
        self.storeCaseSteps()
    }
    
    func changeDateViewValue() {
        self.storeCaseSteps()
    }
}

extension AdminMainViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.storeCaseSteps()
    }
}

