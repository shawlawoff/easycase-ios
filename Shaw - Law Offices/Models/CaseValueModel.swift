//
//  CaseValueModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct CaseValueModel {
    let ref: DatabaseReference?
    let price: String
    let note: String
    
    init(price: String, note: String) {
        self.ref = nil
        self.price = price
        self.note = note
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let price = value["price"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.price = price
        
        if let note = value["note"] as? String {
            self.note = note
        } else {
            self.note = ""
        }
    }
    
    func toAnyObject() -> Any {
        return [
            "price": price,
            "note": note
        ]
    }
}
