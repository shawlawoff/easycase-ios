//
//  ChangePasswordViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/27/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD
import FirebaseAuth

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(logoTapped(tapGestureRecognizer:)))
        imgLogo.isUserInteractionEnabled = true
        imgLogo.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: - User Interaction
    @objc func logoTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let oldPass = txtOldPass.text!
        let newPass = txtNewPass.text!
        let confirmPass = txtConfirmPass.text!
        
        if (oldPass.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your old password")
            return
        }
        
        if (newPass.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your new password")
            return
        }
        
        if (confirmPass.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your confirm password")
            return
        }
        
        if (oldPass != AppManager.sharedInstance.getPassword()) {
            self.showAlert(withTitle: "Warning", message: "Old password is not correct.")
            return
        }
        
        if (newPass != confirmPass) {
            self.showAlert(withTitle: "Warning", message: "The passwords don't match, please enter the passwords correctly.")
            return
        }
        
        let user = Auth.auth().currentUser
        let credential = EmailAuthProvider.credential(withEmail: AppManager.sharedInstance.getEmail(),
                                                      password: AppManager.sharedInstance.getPassword())
        
        KRProgressHUD.show()
        user?.reauthenticateAndRetrieveData(with: credential, completion: { (data, error) in
            if let error = error {
                KRProgressHUD.dismiss()
                self.showAlert(withTitle: "Error", message: error.localizedDescription)
            } else {
                Auth.auth().currentUser?.updatePassword(to: newPass) { (error) in
                    KRProgressHUD.dismiss()
                    if (error == nil) {
                        self.txtOldPass.text = ""
                        self.txtNewPass.text = ""
                        self.txtConfirmPass.text = ""
                        
                        AppManager.sharedInstance.setPassword(password: newPass)
                        self.showAlert(withTitle: "Success", message: "Updated password.")
                    } else {
                        self.showAlert(withTitle: "Error", message: error?.localizedDescription)
                    }
                }
            }
        })
    }
}
