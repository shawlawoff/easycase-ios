//
//  NotificationCell.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import LGButton

protocol NotificationCellDelegate {
    func deleteNotification(_ key: String)
}

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnDelete: LGButton!
    
    var delegate: NotificationCellDelegate? = nil
    var childKey: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteAction(_ sender: Any) {
        
        if delegate != nil {
            delegate?.deleteNotification(childKey)
        }
        
    }
}
