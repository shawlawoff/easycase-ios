//
//  LoginViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD
import FirebaseAuth
import MBRadioCheckboxButton

class LoginViewController: BaseViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var checkRemember: CheckboxButton!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func login(_ email: String, _ password: String) {
        KRProgressHUD.show()
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error == nil {
                AppManager.sharedInstance.setEmail(email: email)
                AppManager.sharedInstance.setPassword(password: password)
                
                let userRef = KDataRefUser.child((Auth.auth().currentUser?.uid)!)
                userRef.observeSingleEvent(of: .value, with: { snapshot in
                    KRProgressHUD.dismiss()
                    if let user = UserModel(snapshot: snapshot) {
                        AppManager.sharedInstance.setName(name: user.name)
                        AppManager.sharedInstance.setAdminStatus(login: user.is_admin)
                        AppManager.sharedInstance.setLoginKeep(login: self.checkRemember.isOn)
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showMainViewController()
                    }
                })
            } else {
                KRProgressHUD.dismiss()
                self.showAlert(withTitle: "Error", message: error?.localizedDescription)
            }
        }
    }

    // MARK: - User InterAction
    
    @IBAction func loginAction(_ sender: Any) {
        
        let password = txtPassword.text!
        let email = txtEmail.text!
        
        if(email.count == 0){
            self.showAlert(withTitle: "Warning", message: "Please enter your email")
            return
        }
        if(password.count == 0){
            self.showAlert(withTitle: "Warning", message: "Please enter your password")
            return
        }

        login(email, password)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
