//
//  BaseViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import MBRadioCheckboxButton

class BaseViewController: UIViewController {
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeControls()
    }
    
    // MARK: Layout
    func initializeControls() {
    }
    
    func getHeight(text: String, width: CGFloat) -> CGFloat {
        let label = UILabel()
        label.text = text
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        let size = CGSize(width: width, height: 1000)
        return label.sizeThatFits(size).height
    }
    
    func showAlert(withTitle title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertConfirm(withTitle title: String, message: String? , isCancel:Bool = false, completion: (() -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //alert.view.tintColor = UIColor.red
        alert.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.red]), forKey: "attributedTitle")
        alert.setValue(NSAttributedString(string: message!, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.red]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            completion!()
        }))
        if(isCancel){
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    func createCheckboxStep(_ text: String) -> CheckboxButton {
        var height = getHeight(text: text, width: UIScreen.main.bounds.width - 30 - 20)
        if height < 25 {
            height = 25
        }
        let newCheckBtn = CheckboxButton(frame: CGRect(x: 0, y: 0, width: 100, height: height))
        newCheckBtn.setTitle(text, for: .normal)
        newCheckBtn.setTitleColor(UIColor.black, for: .normal)
        newCheckBtn.titleLabel?.lineBreakMode = .byWordWrapping
        newCheckBtn.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 15)
        
        newCheckBtn.contentVerticalAlignment = .center
        
        newCheckBtn.translatesAutoresizingMaskIntoConstraints = false
        newCheckBtn.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        return newCheckBtn
    }
    
    func createSingleChoiceView(_ text: String) -> SingleChoiceView {
        let singleView = SingleChoiceView()
        singleView.question = text
        singleView.translatesAutoresizingMaskIntoConstraints = false
        singleView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        return singleView
    }
    
    func createTextField(_ text: String) -> CaseStepTextField {
        let txtField = CaseStepTextField()
        txtField.title = text
        
        return txtField
    }
    
    func createDateView(_ text: String) -> CaseStepDateView {
        let txtField = CaseStepDateView()
        txtField.title = text
        
        return txtField
    }
    
    func convertQuestionToString(_ state: QuestionState) -> String {
        if (state == .yes) {
            return "YES"
        } else if (state == .no) {
            return "NO"
        }
        
        return ""
    }
    
    func convertStringToQuestion(_ state: String) -> QuestionState {
        if (state == "YES") {
            return .yes
        } else if (state == "NO") {
            return .no
        }
        
        return .unknown
    }
}
