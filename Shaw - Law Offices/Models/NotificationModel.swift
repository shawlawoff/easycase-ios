//
//  NotificationModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct NotificationModel {
    let ref: DatabaseReference?
    let message: String
    var isRead: Bool
    
    init(message: String) {
        self.ref = nil
        self.message = message
        self.isRead = false
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let message = value["message"] as? String,
            let isRead = value["isRead"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.message = message
        self.isRead = isRead
    }
    
    func toAnyObject() -> Any {
        return [
            "message": message,
            "isRead": isRead
        ]
    }
}
