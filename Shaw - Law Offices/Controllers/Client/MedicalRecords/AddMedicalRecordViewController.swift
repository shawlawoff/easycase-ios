//
//  AddMedicalRecordViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD
import FirebaseDatabase
import FirebaseStorage

class AddMedicalRecordViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    var user: UserModel!
    var isSelectedImage = false
    var imagePicker = UIImagePickerController()
    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var uploadImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func selectImageAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let strName = txtName.text!
        if (strName.count == 0) {
            showAlert(withTitle: "Warning", message: "Please enter the file name")
            return
        }
        
        if (isSelectedImage) {
            KRProgressHUD.show()
            
            var data = Data()
            data = self.uploadImage.image!.jpegData(compressionQuality: 0.8)!
            // set upload path
            let filePath = "images/\(String(Date().millisecondsSince1970)).jpg"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            let storage = Storage.storage()
            let storageRef = storage.reference()
            
            storageRef.child(filePath).putData(data, metadata: metaData) { (metaData, error) in
                if let error = error {
                    KRProgressHUD.dismiss()
                    self.showAlert(withTitle: "Error", message: error.localizedDescription)
                } else {
                    storageRef.child(filePath).downloadURL { (url, error) in
                        KRProgressHUD.dismiss()
                        guard let downloadURL = url else {
                            return
                        }
                        
                        let recordModel = MedicalRecordModel(name: strName, filepath: downloadURL.absoluteString);
                        let time = Date().millisecondsSince1970
                        KDataRefMedical.child(self.user.uid).child(String(time)).setValue(recordModel.toAnyObject());
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            showAlert(withTitle: "Warning", message: "Please select the image")
        }
    }
    
    // MARK: - ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        uploadImage.image = chosenImage
        isSelectedImage = true
        dismiss(animated: true, completion: nil)
    }
}

