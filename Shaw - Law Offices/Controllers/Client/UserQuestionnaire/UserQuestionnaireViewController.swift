//
//  UserQuestionnaireViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD

class UserQuestionnaireViewController: BaseViewController {

    var user: UserModel!
    
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblClientInfo: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadQuestionnaires()
    }
    
    override func initializeControls() {
        lblUserEmail.text = user.email
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(logoTapped(tapGestureRecognizer:)))
        imgLogo.isUserInteractionEnabled = true
        imgLogo.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func loadQuestionnaires() {
        KRProgressHUD.show()
        KDataRefQuestion.child(user.uid).observe(.value) { (snapshot) in
            KRProgressHUD.dismiss()
            if let questionModel = QuestionnaireModel(snapshot: snapshot) {
                var strInfo = ""
                strInfo += "Name: " + questionModel.name
                strInfo += "\nDate of birth: " + questionModel.birthday
                strInfo += "\nAddress: " + questionModel.address
                strInfo += "\nCity: " + questionModel.city
                strInfo += "\nState: " + questionModel.state
                strInfo += "\nZipcode: " + questionModel.zipcode
                strInfo += "\nCell#: " + questionModel.cell
                strInfo += "\nEmail: " + questionModel.email
                
                strInfo += "\nDo you agree to get info by mail?: " + questionModel.infoByMail
                strInfo += "\nWere you injured?: " + questionModel.injured
                strInfo += "\nDid you take the ambulance?: " + questionModel.ambulance
                strInfo += "\nAre you still treating medically?: " + questionModel.medically
                strInfo += "\nDid you go to er?: " + questionModel.er
                strInfo += "\nWhich hospital: " + questionModel.hospital
                strInfo += "\nHave you lost wages?: " + questionModel.wages
                strInfo += "\nIs workers comp involved?: " + questionModel.involved
                strInfo += "\nDo you have health insurance?: " + questionModel.insurance
                strInfo += "\nDate of incident: " + questionModel.dateOfIncident
                strInfo += "\nTime: " + questionModel.time
                strInfo += "\nCity: " + questionModel.insuranceCity
                strInfo += "\nWere you t fault?: " + questionModel.fault
                strInfo += "\nDid you mention it being your fault?: " + questionModel.yourfault
                strInfo += "\nWas there property damage?: " + questionModel.damage
                strInfo += "\nWhats the estimated damage?: " + questionModel.estimatedDamage
                strInfo += "\nDo you have photos of car after wreck?: " + questionModel.wreck
                
                self.lblClientInfo.text = strInfo
            }
        }
    }
    

    // MARK: - User Interaction
    @objc func logoTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
