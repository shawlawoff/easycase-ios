//
//  ValueHistoryView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/9/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import LGButton

protocol ValueHistoryViewDelegate {
    func delete(_ key: String, _ model: HistoryModel)
}

class ValueHistoryView: CustomView {

    @IBOutlet weak var imgMark: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var btnDelete: LGButton!
    
    var historyModel: HistoryModel!
    var childKey: String!
    
    var delegate: ValueHistoryViewDelegate? = nil
    
    override func prepare() {
        super.prepare()
    }

    @IBAction func deleteAction(_ sender: Any) {
        if (delegate != nil) {
            delegate?.delete(childKey, historyModel)
        }
    }
}
