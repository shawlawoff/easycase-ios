//
//  RegisterViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD
import FirebaseAuth

class RegisterViewController: BaseViewController {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (AppManager.sharedInstance.getLoginKeep()) {
            login(AppManager.sharedInstance.getEmail(), AppManager.sharedInstance.getPassword())
        }
    }
    
    func login(_ email: String, _ password: String) {
        KRProgressHUD.show()
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error == nil {
                let userRef = KDataRefUser.child((Auth.auth().currentUser?.uid)!)
                userRef.observeSingleEvent(of: .value, with: { snapshot in
                    KRProgressHUD.dismiss()
                    if let user = UserModel(snapshot: snapshot) {
                        AppManager.sharedInstance.setName(name: user.name)
                        AppManager.sharedInstance.setAdminStatus(login: user.is_admin)
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showMainViewController()
                    }
                })
            } else {
                KRProgressHUD.dismiss()
                self.showAlert(withTitle: "Error", message: error?.localizedDescription)
            }
        }
    }

    // MARK: - User Interaction
    @IBAction func registerAction(_ sender: Any) {
        let email = txtEmail.text!
        let name = txtName.text!
        let password = txtPassword.text!
        let confirm = txtConfirmPass.text!
        
        if (email.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your email")
            return
        }
        if (name.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your name")
            return
        }
        if (password.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your pin")
            return
        }
        if (confirm.count == 0) {
            self.showAlert(withTitle: "Warning", message: "Please enter your confirm pin")
            return
        }
        if (password != confirm) {
            self.showAlert(withTitle: "Warning", message: "Confirm pin not match")
            return
        }
        
        KRProgressHUD.show()
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            KRProgressHUD.dismiss()
            if(error == nil){
                let uid = (Auth.auth().currentUser?.uid)!
                
                let userModel = UserModel(uid: uid, email: email, name: name)
                KDataRefUser.child(uid).setValue(userModel.toAnyObject())
                
                self.txtEmail.text = ""
                self.txtName.text = ""
                self.txtPassword.text = ""
                self.txtConfirmPass.text = ""
                
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)
                }
                
                self.performSegue(withIdentifier: Segue.goLogin, sender: self)
            } else {
                self.showAlert(withTitle: "Error", message: error?.localizedDescription)
            }
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.performSegue(withIdentifier: Segue.goLogin, sender: self)
    }
    
}
