//
//  ChatContainerViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class ChatContainerViewController: UIViewController {

    @IBOutlet weak var containerContentView: UIView!
    
    var otherId: String? = nil
    var otherName: String!
    
    let chatVC = ChatViewController()
    
    /// Required for the `MessageInputBar` to be visible
    override var canBecomeFirstResponder: Bool {
        return chatVC.canBecomeFirstResponder
    }
    
    /// Required for the `MessageInputBar` to be visible
    override var inputAccessoryView: UIView? {
        return chatVC.inputAccessoryView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        chatVC.otherName = otherName
        chatVC.otherId = otherId
        
        chatVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addChild(chatVC)
        containerContentView.addSubview(chatVC.view)
        chatVC.didMove(toParent: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 29/255.0, green: 89/255.0, blue: 140/255.0, alpha: 1.0)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        chatVC.view.leadingAnchor.constraint(equalTo: containerContentView.leadingAnchor).isActive = true
        chatVC.view.trailingAnchor.constraint(equalTo: containerContentView.trailingAnchor).isActive = true
        chatVC.view.bottomAnchor.constraint(equalTo: containerContentView.bottomAnchor).isActive = true
        chatVC.view.topAnchor.constraint(equalTo: containerContentView.topAnchor).isActive = true
    }    
}
