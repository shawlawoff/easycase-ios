//
//  NotificationViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import PopupDialog
import FirebaseAuth
import FirebaseDatabase

class NotificationViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAddHeight: NSLayoutConstraint!
    
    var user: UserModel!
    var notifications = [NotificationModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        
        if !AppManager.sharedInstance.getAdminStatus() {
            btnAddHeight.constant = 0
        }
        
        KDataRefNotify.child(user.uid).queryOrderedByKey().observe(.value) { (snapshot) in
            self.notifications.removeAll()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = NotificationModel(snapshot: snapshot) {
                    self.notifications.append(item)
                }
            }
            self.tblView.reloadData()
        }
    }

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let inputVC = storyboard.instantiateViewController(withIdentifier: "AddNotificationViewController") as! AddNotificationViewController
        inputVC.user = user
        
        let inputVCPopup = PopupDialog(viewController: inputVC)
        self.present(inputVCPopup, animated: true, completion: nil)
    }
}

extension NotificationViewController: UITableViewDataSource, UITableViewDelegate, NotificationCellDelegate {
    func deleteNotification(_ key: String) {
        KDataRefNotify.child(user.uid).child(key).removeValue()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        let notification = notifications[notifications.count - indexPath.row - 1]
        
        cell.lblContent.text = notification.message
        cell.childKey = notification.ref?.key
        
        let date = Date(milliseconds: Int64(cell.childKey)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        cell.lblTime.text = dateFormatter.string(from: date)
        
        if AppManager.sharedInstance.getAdminStatus() {
            cell.delegate = self
            cell.btnDelete.isHidden = false
        } else {
            cell.btnDelete.isHidden = true
        }
        
        if !notification.isRead {
            cell.lblContent.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        } else {
            cell.lblContent.font = UIFont(name:"HelveticaNeue-Regular", size: 16.0)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailNotificationViewController") as! DetailNotificationViewController
        detailVC.notification = notifications[notifications.count - indexPath.row - 1]
        
        let popup = PopupDialog(viewController: detailVC)
        self.present(popup, animated: true, completion: nil)
    }
}
