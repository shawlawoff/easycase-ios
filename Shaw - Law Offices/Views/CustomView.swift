//
//  CustomView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class CustomView: UIView {
    
    var contentView: UIView?
    
    // Loads instance from nib with the same name.
    func loadNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        self.backgroundColor = UIColor.clear
        if contentView != nil {
            return
        }
        
        guard let view = loadNib() else {
            return
        }
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
        
        contentView = view
        
        prepare()
    }
    
    func prepare() {
        
    }
}
