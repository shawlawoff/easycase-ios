//
//  CaseStepDateView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

protocol CaseStepDateViewDelegate {
    func changeDateViewValue()
}

@IBDesignable
class CaseStepDateView: CustomView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtContent: UITextField!
    
    var delegate: CaseStepDateViewDelegate? = nil
    
    @IBInspectable
    var title: String? {
        get {
            return lblTitle.text
        }
        set {
            lblTitle.text = newValue
        }
    }
    
    var content: String? {
        get {
            return txtContent.text
        }
        set {
            txtContent.text = newValue
        }
    }
    
    override func prepare() {
        super.prepare()
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        txtContent.inputView = datePickerView
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        txtContent.text = dateFormatter.string(from: sender.date)
        
        if delegate != nil {
            delegate?.changeDateViewValue()
        }
    }
}
