//
//  SettingModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation

struct SettingModel {
    
    let sid: Int
    let name: String
    
    init(sid: Int, name: String) {
        
        self.sid = sid
        self.name = name
        
    }
    
    func toAnyObject() -> Any {
        return [
            "sid": sid,
            "name": name
        ]
    }
}
