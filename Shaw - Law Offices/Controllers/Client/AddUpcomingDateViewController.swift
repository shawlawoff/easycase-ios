//
//  AddUpcomingDateViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class AddUpcomingDateViewController: BaseViewController, UITextFieldDelegate {

    var user: UserModel!
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initializeControls() {
        txtDate.delegate = self
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        txtDate.inputView = datePickerView
    }
    
    // MARK: - User Interaction
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        txtDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let strDate = txtDate.text!
        let strDesc = txtDescription.text!
        if (strDate.count == 0 || strDesc.count == 0) {
            showAlert(withTitle: "Warning", message: "Please enter all information")
            return
        }
        
        let dateModel = UpcomingDateModel(date: strDate, note: strDesc)
        let time = Date().millisecondsSince1970
        KDataRefUpcoming.child(user.uid).child(String(time)).setValue(dateModel.toAnyObject())
        
        self.dismiss(animated: true, completion: nil)
    }
}
