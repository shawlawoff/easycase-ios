//
//  SettingsViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var settingList:[SettingModel] = [SettingModel(sid: 1, name: "Change Password"),
                                      SettingModel(sid: 2, name: "Logout")]
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.tableFooterView = UIView()
    }
    
    // MARK: - User Interaction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        let item = settingList[indexPath.row]
        
        cell.lblTitle.text = item.name
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = settingList[indexPath.row]
        switch item.sid {
        case 1:             // change password
            self.performSegue(withIdentifier: Segue.goChangePass, sender: self)
            break
        case 2:             // logout
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            AppManager.sharedInstance.setLoginKeep(login: false)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showLoginViewController()
            break
        default:
            break
        }
        
    }
    
}
