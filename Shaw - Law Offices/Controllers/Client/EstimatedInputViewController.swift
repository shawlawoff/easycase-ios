//
//  EstimatedInputViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class EstimatedInputViewController: BaseViewController {

    var user: UserModel!
    var previousValue: Int64!
    var isPositive: Bool!
    
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func sendPushNotification() {
        let json: [String: Any] = ["to": user.regToken,
                                   "notification" : [
                                    "body" : txtDescription.text! + (self.isPositive ? " +$" : "-$") + String(txtValue.text!),
                                        "title": (self.isPositive ? "Good news!" : "Bad news!")
                                    ],
                                   "data" : [
                                    "body" : txtDescription.text! + (self.isPositive ? " +$" : "-$") + String(txtValue.text!),
                                    "title": "EZ-Case"]
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("key=AAAA8DcETH0:APA91bHe6aTEenACTEjl1fOK7BO0xHF2nIYNYfaMRr2OhtD56L8p4lnJYjrGMs85Vi0ol_h2K7bwpH-_rALOv1kVYc_DmarOhcD3El3WUtlBEu2s9wOnSnz_3CsIKIIjxxO26LnLlYLH", forHTTPHeaderField: "Authorization")
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }
        
        task.resume()
    }
    

    // MARK: - User Interaction
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let strValue = txtValue.text!
        let strDescription = txtDescription.text!
        if (strValue.count == 0 || strDescription.count == 0) {
            showAlert(withTitle: "Warning", message: "Please enter all information")
            return
        }
        
        let newValue = Int64(strValue)!
        var strDetail = ""
        
        if (isPositive) {
            strDetail = strDescription + " +$" + String(newValue)
            KDataRefValue.child(user.uid).child("price").setValue(String(previousValue + newValue))
        } else {
            strDetail = strDescription + " -$" + String(newValue)
            KDataRefValue.child(user.uid).child("price").setValue(String(previousValue - newValue))
        }
        
        let time = Date().millisecondsSince1970
        let historyModel = HistoryModel(percent: 0, detail: strDetail, createAt: time, increase: isPositive)
        KDataRefHistory.child(user.uid).child(String(time)).setValue(historyModel.toAnyObject())
        
        self.sendPushNotification()
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
