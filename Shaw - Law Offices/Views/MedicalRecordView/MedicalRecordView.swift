//
//  MedicalRecordView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

protocol MedicalRecordDelegate {
    func itemClicked(filepath: String)
}

class MedicalRecordView: CustomView {

    @IBOutlet weak var lblContent: UILabel!
    var delegate: MedicalRecordDelegate? = nil
    var filepath: String!
    
    override func prepare() {
        super.prepare()
    }
    
    func setupDetail(_ item: MedicalRecordModel) {
        let textRange = NSMakeRange(0, item.name.count)
        let attributedText = NSMutableAttributedString(string: item.name)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        
        self.lblContent.attributedText = attributedText
        self.filepath = item.filepath
    }

    @IBAction func clickAction(_ sender: Any) {
        if (delegate != nil) {
            delegate?.itemClicked(filepath: filepath)
        }
    }
}
