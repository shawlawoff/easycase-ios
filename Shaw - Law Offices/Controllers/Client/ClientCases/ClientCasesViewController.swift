//
//  ClientCasesViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import KRProgressHUD
import FirebaseDatabase

class ClientCasesViewController: BaseViewController {

    @IBOutlet weak var tblView: UITableView!
    var clientCases: [UserModel] = []
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.tableFooterView = UIView()
        
        loadClientCases()
    }
    
    func loadClientCases() {
        KRProgressHUD.show()
        KDataRefUser.observe(.value) { (snapshot) in
            KRProgressHUD.dismiss()
            self.clientCases.removeAll()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let item = UserModel(snapshot: snapshot),
                    !item.is_admin,
                    !item.deleted {
                    self.clientCases.append(item)
                }
            }
            self.tblView.reloadData()
        }
    }
}

extension ClientCasesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clientCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientCaseCell", for: indexPath) as! ClientCaseCell
        cell.uid = self.clientCases[indexPath.row].uid
        cell.lblName.text = self.clientCases[indexPath.row].name
        cell.lblEmail.text = self.clientCases[indexPath.row].email
        
        cell.delegate = self
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let questionnaireVC = storyboard.instantiateViewController(withIdentifier: "UserQuestionnaireViewController") as! UserQuestionnaireViewController
//        questionnaireVC.user = clientCases[indexPath.row]
        let adminMainVC = storyboard.instantiateViewController(withIdentifier: "AdminMainViewController") as! AdminMainViewController
        adminMainVC.user = clientCases[indexPath.row]
        
        self.navigationController?.pushViewController(adminMainVC, animated: true)
    }
}

extension ClientCasesViewController: ClientCaseCellDelegate {
    func deleteAccount(_ uid: String) {
        
        self.showAlertConfirm(withTitle: "Ez-Case", message: "Do you want to delete this account?",
                              isCancel: true) {
                                KDataRefUser.child(uid).child("deleted").setValue(true)
        }
    }
}
