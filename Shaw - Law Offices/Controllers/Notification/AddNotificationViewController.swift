//
//  AddNotificationViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/24/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class AddNotificationViewController: BaseViewController {

    @IBOutlet weak var txtMessage: UITextView!
    
    var user: UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtMessage.layer.borderColor = UIColor.black.cgColor
        txtMessage.layer.borderWidth = 0.5
    }
    

    // MARK: - User Interaction

    @IBAction func saveAction(_ sender: Any) {
        
        let message = txtMessage.text!
        if message.count == 0 {
            showAlert(withTitle: "Warning", message: "Please enter your message")
            return
        }
        
        KDataRefNotify.child(user.uid).child("\(Date().millisecondsSince1970)")
            .setValue(NotificationModel(message: message).toAnyObject())
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
