//
//  UpcomingDateView.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import LGButton

protocol UpcomingDateViewDelegate {
    func deleteUpcomingDate(_ key: String)
}

class UpcomingDateView: CustomView {
    
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var btnDelete: LGButton!
    
    var childKey: String!
    var delegate: UpcomingDateViewDelegate? = nil
    
    override func prepare() {
        super.prepare()
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        if (delegate != nil) {
            delegate?.deleteUpcomingDate(childKey)
        }
    }
}
