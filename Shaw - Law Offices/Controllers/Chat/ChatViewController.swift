//
//  ChatViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MessageKit
import MessageInputBar

class ChatViewController: MessagesViewController {

    let sender = Sender(id: "any_unique_id", displayName: "Steven")
    var messages: [MessageModel] = []
    var otherId: String? = nil
    var senderId: String!
    var otherName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
        }
        
        messageInputBar.delegate = self
        
        senderId = Auth.auth().currentUser?.uid
        loadMessages()
    }
    
    func loadMessages() {
        if (otherId == nil) {
            otherId = senderId
        }
        KDataRefChat.child(otherId!).observe(.childAdded) { (snapshot) in
            if let chatMessage = ChatMessage(snapshot: snapshot) {
                var message = MessageModel(text: chatMessage.message,
                                           sender: Sender(id: chatMessage.senderId, displayName: AppManager.sharedInstance.getName()),
                                           messageId: chatMessage.id,
                                           date: Date(milliseconds: chatMessage.createAt))
                if (self.senderId != chatMessage.senderId) {
                    message = MessageModel(text: chatMessage.message,
                                           sender: Sender(id: chatMessage.senderId, displayName: self.otherName),
                                           messageId: chatMessage.id,
                                           date: Date(milliseconds: chatMessage.createAt))
                    KDataRefChat.child(self.otherId!).child(String(chatMessage.createAt)).child("is_read").setValue(true)
                }
                self.insertMessage(message)
            }
        }
    }
    
    func insertMessage(_ message: MessageModel) {
        messages.append(message)
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messages.count - 1])
            if messages.count >= 2 {
                messagesCollectionView.reloadSections([messages.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }
    
    func isLastSectionVisible() -> Bool {
        
        guard !messages.isEmpty else { return false }
        
        let lastIndexPath = IndexPath(item: 0, section: messages.count - 1)
        
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
}

extension ChatViewController: MessagesDataSource {
    
    func currentSender() -> Sender {
        return Sender(id: senderId, displayName: AppManager.sharedInstance.getName())
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.isHidden = true
    }
}

extension ChatViewController: MessagesDisplayDelegate, MessagesLayoutDelegate {}

extension ChatViewController: MessageInputBarDelegate {
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        messageInputBar.inputTextView.text = ""
        messageInputBar.inputTextView.resignFirstResponder()
        
        let createdAt = Date().millisecondsSince1970
        let chatMessage = ChatMessage(id: String(createdAt), senderId: senderId,
                                      message: text, createAt: createdAt,
                                      is_read: false)
        KDataRefChat.child(otherId!).child(String(createdAt)).setValue(chatMessage.toAnyObject())
    }
}
