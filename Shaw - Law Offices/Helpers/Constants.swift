//
//  Constants.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

//Firebase Database Reference

let KDataRefUser = Database.database().reference(withPath: "users")
let KDataRefQuestion = Database.database().reference(withPath: "questionnaires")
let KDataRefValue = Database.database().reference(withPath: "casevalues")
let KDataRefCaseType = Database.database().reference(withPath: "casetypes")
let KDataRefClaim = Database.database().reference(withPath: "feeds")
let KDataRefLawsuit = Database.database().reference(withPath: "lawsuit")
let KDataRefHistory = Database.database().reference(withPath: "history")
let KDataRefUpcoming = Database.database().reference(withPath: "upcomingDates")
let KDataRefMedical = Database.database().reference(withPath: "medicalRecords")
let KDataRefChat = Database.database().reference(withPath: "chatMessages")
let KDataRefNotify = Database.database().reference(withPath: "notifications")

enum Config {
    static let baseURL = NSURL(string: "http://www.example.org/")!
}

enum Color {
    static let primaryColor = UIColor(red: 1/255.0, green: 161/255.0, blue: 76/255.0, alpha: 1.0)
    static let secondaryColor = UIColor.lightGray
    
    static let greenRingColor = UIColor(red: 0/255.0, green: 215/255.0, blue: 83/255.0, alpha: 1.0)
}

enum Segue {
    static let goLogin = "goLogin"
    static let goRegister = "goRegister"
    static let goUserMain = "goUserMain"
    static let goQuestionnaire = "goQuestionnaire"
    static let goChangePass = "goChangePass"
}

let KUserDefaultTerms =  "terms"
let KUserDefaultEmail =  "email"
let KUserDefaultPassword =  "password"
let KUserDefaultName = "name"
let KUserDefaultLogin = "login"
let KUserDefaultAdmin =  "adminstatus"

let KTextCaseSteps = ["1. Intake Form",
    "2. Client interview",
    "3. Employment contract",
    "4. Acquire client signature",
    "5. Initial confirmation letter",
    "6. Acquire incident report",
    "7. Place claim on statue of limitations office calendar",
    "8. Certified letter of representation to request documents",
    "9. Prepare witness ledger and send witness letters",
    "10. Prepare police ledger and send officer ledger",
    "11. Referring attorney letter with fee division agreement",
    "12. Vehicle purchase letter and repairs",
    "13. Visit scene by attorney for photos or do google earth",
    "14. If in lawsuit, file appearance with court and move to from claim tracking to lawsuit tracking. Send letter to former attorney asking for proof of all liens held by him",
    "15. Letter/email to client requesting all documents and photos from client and request list of medical providers from client with relevant preexisting treatment in same letter",
    "16. Obtain health/ medical insurance information in same letter",
    "17. Optain HIPAA/Tax/Employment signed authorizations from client in same letter",
    "18. Letter of representation to health/medical insurance",
    "19. Request medical records and bills from all providers (includes pre existing)",
    "20. Request wage information from client’s employer",
    "21. Letter to medical/health insurance asking for lien info",
    "22. Compile completed medical records and bills",
    "23. Request doctors narrative",
    "24. Provide a receipt of doctors narrative",
    "25. Prepare attorney letter of protection if requested by client",
    "26. Prepare litigation loan request to lumberjack, if requested by client",
    "27. Obtain client consent to settle and confirm treatment completed/ready by email/mail/fax",
    "28. Determine/calculate ALL liens in file and make lien ledger",
    "29. Prepare demand pacage to insurance adjuster",
    "30. Send demand package to insurance adjuster and client",
    "31. Confirm receipt by unsurance adjuster by email",
    "32. Receive counter offer or rejection of claim",
    "33. Send client rejection or offter to settle by email/mail include lien ledger and in-pocket estimate for client",
    "34. After client accepts, prepare distribution sheet with all estimated liens and expenses listed on expense ledger",
    "35. Send distribution sheet to client ( must include permission to sign clients name to settlement funds; must include information that rejction will result in a filing of a lawsuit)",
    "36. After signed distribution sheet received from client, then inform insurance adjuster in writing that offer is accepted",
    "37. If distribution sheet rejected, prepare for lawsuit stage (move claim to lawsuit tracker)",
    "38. If accepted, confirm acceptance to insurance adjuster by mail/email/fax (with copy to client) and request release and checks mailed to valpo office",
    "39. Receive release from insurance",
    "40. Send signed release to insurance adjuster by mail/email/fax with copy to client",
    "41. Receive checks",
    "42. Deposit checks in trust account and wait for clear ",
    "43. Send checks to all lienholders with proof to client",
    "44. Transfer funds to client (wire, in person, mail per the distribution sheet indicated preference)",
    "45. Confirm lienholders received check",
    "46. Thank you letter to client with representation concluded info",
    "47. Transfer attorneys fees from trust account to Law Firm Operating account"]

let KTextLawSuiteTitles = ["First Steps", "Pre-suit", "Filing and Service", "Responsive Pleadings", "Holy Trinity (Initial Case Management Hearing)",
    "CMO Dates", "Client Meetings", "Lay Witnesses", "Damages/Liens", "Written Discovery to Us", "Protective Orders", "Written Discovery to Them",
    "Motions to Compel", "Oral Discovery", "IMEs", "Expert Witness", "MSJ", "Daubert", "Request Mediation", "Exhibits Prep",
    "Witness Trial Prep", "Voir Dire", "MILs", "Jury Instructions", "Trial Motions"]
let KTextLawSuite = [
    // First Steps
    [CaseStepModel("1. Initial Interview with Client", .checkbox),
     CaseStepModel("2. Contract Placed in File", .checkbox),
     CaseStepModel("3. Send LOR and Preservation to Defendant/Insurance Company", .checkbox),
     CaseStepModel("4. Get Copies of Documents from Client", .checkbox),
     CaseStepModel("5. Inspect Scene", .checkbox),
     CaseStepModel("6. Get any Police/Crash Reports or Prior Records", .checkbox),
     CaseStepModel("7. Get Photos from Client or Others", .checkbox),
     CaseStepModel("8. Scan Google Earth Photos of Scene", .checkbox),
     CaseStepModel("9. Get List of all Medical Providers", .checkbox),
     CaseStepModel("10. Get HIPAA Signed", .checkbox),
     CaseStepModel("11. Send Witness Letter", .checkbox),
     CaseStepModel("12. Send Police Letter", .checkbox),
     CaseStepModel("13. Send Car Repair Estimate Letter", .checkbox)],
    
    // Pre-suit
    [CaseStepModel("1. Statute of Limitations Placed on Calendar", .text),
     CaseStepModel("2. Located Legal Name and Address of OC", .text),
     CaseStepModel("3. Governmental Notices Prepared", .checkbox),
     CaseStepModel("4. Governmental Notices Certified Mailed", .checkbox),
     CaseStepModel("5. Jurisdiction and Venue Determined", .text)],

    // FilingAndService
    [CaseStepModel("1. Prepare Complaint, Appearance and Summons", .checkbox),
     CaseStepModel("2. File on DoxPop", .checkbox),
     CaseStepModel("3. Send Certified Mail of Filings to Defendant", .checkbox),
     CaseStepModel("4. Receive Certified Mailing Proof", .checkbox),
     CaseStepModel("5. File Proof of Service on DoxPop", .checkbox),
     CaseStepModel("6. If No Service, File Service by Publication", .checkbox),
     CaseStepModel("7. If Service but No Answer within 23 days File Default Judgment", .checkbox)],

    // Responsive Pleadings
    [CaseStepModel("1. Receipt and Review of Answer", .checkbox),
     CaseStepModel("2. Third Party Complaint, Summons, Prepared", .checkbox),
     CaseStepModel("3. File on DoxPop", .checkbox),
     CaseStepModel("4. Send Certified Mail of Filings to Defendant", .checkbox),
     CaseStepModel("5. Receive Certified Mailing Proof", .checkbox),
     CaseStepModel("6. File Proof of Service on DoxPop", .checkbox),
     CaseStepModel("7. If No Service, File Service by Publication", .checkbox),
     CaseStepModel("8. If Service but No Answer within 23 days File Default Judgment", .checkbox)],

    // Holy Trinity (Initial Case Management Hearing)
    [CaseStepModel("1. Holy Trinity Motion Prepared", .checkbox),
     CaseStepModel("2. Holy Trinity Filed", .checkbox),
     CaseStepModel("3. Holy Trinity Hearing Placed on Calendar", .checkbox),
     CaseStepModel("4. Call CourtCall to Schedule Telephonic", .checkbox),
     CaseStepModel("5. Case Schedule Hearing Held", .checkbox),
     CaseStepModel("6. Dates Placed on Calendar", .checkbox)],

    // CMO Dates
    [CaseStepModel("1. CMO Request (Cert. of Readiness) Prepared", .checkbox),
     CaseStepModel("2. CMO Request Filed", .checkbox),
     CaseStepModel("3. CMO Hearing Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("4. CMO Hearing Held", .text),
     CaseStepModel("5. CMO Deadlines Issued", .checkbox),
     CaseStepModel("6. DCO Date Placed on Calendar", .text),
     CaseStepModel("7. WCE List Placed on Calendar", .text),
     CaseStepModel("8. WCE List Prepared", .checkbox),
     CaseStepModel("9. WCE List Sent to OC", .checkbox),
     CaseStepModel("10. Expert Disclosure Placed on Calendar", .text),
     CaseStepModel("11. Expert Disclosures/Report Sent to OC", .checkbox),
     CaseStepModel("12. MSJ Deadline", .text),
     CaseStepModel("13. MIL and Instruction Deadline", .text),
     CaseStepModel("14. Final PTC Hearing", .text),
     CaseStepModel("15.TBJ Dates", .text)],

    // Client Meetings
    [CaseStepModel("1. 2nd Client Interview", .checkbox),
     CaseStepModel("2. Prepare Supplemental Damages/Medical Providers", .checkbox),
     CaseStepModel("3. Give Client To-Do List", .checkbox)],

    // Lay Witnesses
    [CaseStepModel("1. Write, Call or Meet Lay Witnesses", .checkbox),
     CaseStepModel("2. Prepare Summary of Testimony", .checkbox),
     CaseStepModel("3. Schedule Deposition of Lay Witness", .checkbox)],

    // Damages/Liens
    [CaseStepModel("1. Get Signed HIPAA", .checkbox),
     CaseStepModel("2. Request Medical Records Compex", .checkbox),
     CaseStepModel("3. Get Signed IRS Auth.", .checkbox),
     CaseStepModel("4. Request Employer Lost Wages", .checkbox),
     CaseStepModel("5. Request Auto/Health MedPay Log with Bills", .checkbox),
     CaseStepModel("6. Prepare Summary Table Medical Expenses", .checkbox),
     CaseStepModel("7. Receive Lien/Note to File", .checkbox),
     CaseStepModel("8. Sign Medicare w/Massive", .checkbox),
     CaseStepModel("9. Do Letters of Protection", .binary)],

    // Written Discovery to Us
    [CaseStepModel("1. Interrogatories Received", .textdate),
     CaseStepModel("2. Interrogatories Sent to Client", .checkbox),
     CaseStepModel("3. Draft Answers to Ints. Received from Client", .checkbox),
     CaseStepModel("4. Final Answers Prepared", .checkbox),
     CaseStepModel("5. Final Answers Sent to OC", .checkbox),
     CaseStepModel("6. RFP Received from Defendant", .textdate),
     CaseStepModel("7. RFP Sent to Client", .checkbox),
     CaseStepModel("8. RFA Received", .checkbox),
     CaseStepModel("9. RFA Due Date Placed on Calendar", .text),
     CaseStepModel("10. RFA Answers Prepared", .checkbox),
     CaseStepModel("11. RFA Answers Served on OC", .text),
     CaseStepModel("12. Supplemental Discovery Received", .checkbox),
     CaseStepModel("13. Supplemental Discovery Sent to Client", .checkbox),
     CaseStepModel("14. Supplemental Answers Received", .checkbox),
     CaseStepModel("15. Supplemental Answers Prepared", .checkbox),
     CaseStepModel("16. Supplemental Answers Sent to OC", .text)],
    
    // Protective Orders
    [CaseStepModel("1. PO Received", .checkbox),
     CaseStepModel("2. PO Reply Brief Prepared", .checkbox),
     CaseStepModel("3. PO Hearing Placed on Calendar", .checkbox),
     CaseStepModel("4. PO Hearing Held", .checkbox)],
    
    // Written Discovery to Them
    [CaseStepModel("1. Interrogatories Drafted", .checkbox),
     CaseStepModel("2. Interrogatories Sent and Due Date Placed on Calendar", .checkbox),
     CaseStepModel("3. Answers Received/Reviewed", .checkbox),
     CaseStepModel("4. RFP Prepared", .checkbox),
     CaseStepModel("5. RFP Sent", .checkbox),
     CaseStepModel("6. RFA Due Date Placed on Calendar", .text),
     CaseStepModel("7. RFP Answers Received/Reviewed", .checkbox),
     CaseStepModel("8. RFA Prepared", .checkbox),
     CaseStepModel("9. RFA Sent", .checkbox),
     CaseStepModel("10. RFA Due Date Placed on Calendar", .text),
     CaseStepModel("11. RFA Answers Received/Reviewed", .checkbox),
     CaseStepModel("12. RFA Admitted Motion (If No Timely Reply) Prepared", .checkbox),
     CaseStepModel("13. RFA Hearing Scheduled on Calendar", .checkbox),
     CaseStepModel("14. RFA Hearing Brief Prepared and Filed", .checkbox),
     CaseStepModel("15. RFA Hearing Held", .checkbox),
     CaseStepModel("16. Supplemental Discovery Prepared", .checkbox),
     CaseStepModel("17. Supplemental Discovery Sent", .checkbox),
     CaseStepModel("18. Supplemental Due Date Placed on Calendar", .checkbox),
     CaseStepModel("19. Supplemental Answers Received/Reviewed", .checkbox)],
    
    // Motions to Compel
    [CaseStepModel("1. Motion to Compel Prepared", .checkbox),
     CaseStepModel("2. MTC Filed with Court", .checkbox),
     CaseStepModel("3. MTC Hearing Scheduled", .checkbox),
     CaseStepModel("4. MTC Hearing Held", .checkbox)],
    
    // Oral Discovery
    [CaseStepModel("1. Deposition of Plaintiff Scheduled and Placed on Calendar", .text),
     CaseStepModel("2. Deposition Done", .checkbox),
     CaseStepModel("3. Transcript Received", .checkbox),
     CaseStepModel("4. Bill Paid", .checkbox),
     CaseStepModel("5. Deposition of Defendant Scheduled and Placed on Calendar", .text),
     CaseStepModel("6. Deposition Done", .checkbox),
     CaseStepModel("7. Transcript Received", .checkbox),
     CaseStepModel("8. Bill Paid", .checkbox),
     CaseStepModel("9. Deposition of Police Witness Scheduled and Placed on Calendar", .text),
     CaseStepModel("10. Deposition Done", .checkbox),
     CaseStepModel("11. Transcript Received", .checkbox),
     CaseStepModel("12. Bill Paid", .checkbox),
     CaseStepModel("13. Deposition of Lay Witness Scheduled and Placed on Calendar", .text),
     CaseStepModel("14. Deposition Done", .checkbox),
     CaseStepModel("15. Transcript Received", .checkbox),
     CaseStepModel("16. Bill Paid", .checkbox),
     CaseStepModel("17. Deposition of OC Liability Expert Witness Scheduled and Placed on Calendar", .text),
     CaseStepModel("18. Deposition Done", .checkbox),
     CaseStepModel("19. Transcript Received", .checkbox),
     CaseStepModel("20. Bill Paid", .checkbox),
     CaseStepModel("21. Deposition of Our Expert Liability Witness Scheduled and Placed on Calendar", .text),
     CaseStepModel("22. Deposition Done", .checkbox),
     CaseStepModel("23. Transcript Received", .checkbox),
     CaseStepModel("24. Bill Paid", .checkbox),
     CaseStepModel("25. Deposition of Treating Physician Scheduled and Placed on Calendar", .text),
     CaseStepModel("26. If Evidentiary, Prepare Doctor Deposition Questions", .checkbox),
     CaseStepModel("27. If Evidentiary, Find/Serve Doc Videos to Court Reporter", .checkbox),
     CaseStepModel("28. If Evidentiary, Find/Serve Doc Videos to OC", .checkbox),
     CaseStepModel("29. Deposition Done", .checkbox),
     CaseStepModel("30. Transcript Received", .checkbox),
     CaseStepModel("31. Bill Paid", .checkbox),
     CaseStepModel("32. Deposition of OC Expert Doctor Witness Scheduled and Placed on Calendar", .text),
     CaseStepModel("33. Deposition Done", .checkbox),
     CaseStepModel("34. Transcript Received", .checkbox),
     CaseStepModel("35. Bill Paid", .checkbox),
     CaseStepModel("36. Deposition of Economist/Damages Witness Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("37. Deposition Done", .checkbox),
     CaseStepModel("38. Transcript Received", .checkbox),
     CaseStepModel("39. Bill Paid", .checkbox),
     CaseStepModel("40. 30b6 Deposition Notice Prepared and Served on OC", .checkbox),
     CaseStepModel("41. 30b6 Deposition PO Responses Prepared and Filed with Court", .checkbox),
     CaseStepModel("42. 30b6 Deposition Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("43. Deposition Done", .checkbox),
     CaseStepModel("44. Transcript Received", .checkbox),
     CaseStepModel("45. Bill Paid", .checkbox)],
    
    // IMEs
    [CaseStepModel("1. IME Received", .checkbox),
     CaseStepModel("2. IME Objection Prepared", .checkbox),
     CaseStepModel("3. IME Objection Filed", .checkbox),
     CaseStepModel("4. IME Objection Hearing Held", .text),
     CaseStepModel("5. IME Placed on Calendar", .checkbox),
     CaseStepModel("6. IME Attended", .checkbox),
     CaseStepModel("7. IME Report Received", .checkbox),
     CaseStepModel("8. IME Forwarded to Our Expert", .checkbox)],
    
    // Expert Witness
    [CaseStepModel("1. Search/Find Liability Experts", .text),
     CaseStepModel("2. Call/E-mail Potential Experts", .text),
     CaseStepModel("3. Acquire CV/Engagement Letter from Expert", .checkbox),
     CaseStepModel("4. Pay Expert", .checkbox),
     CaseStepModel("5. Prepare Expert Summary", .checkbox),
     CaseStepModel("6. Send Expert Materials", .checkbox),
     CaseStepModel("7. Request Expert Written Report", .checkbox),
     CaseStepModel("8. Written Disclosures of Expert Due Date", .text),
     CaseStepModel("9. Disclosures Received/Reviewed", .checkbox),
     CaseStepModel("10. Disclosures of Expert Filed with Court", .checkbox)],
    
    // MSJ
    [CaseStepModel("1. MSJ Received", .checkbox),
     CaseStepModel("2. MSJ Brief Prepared", .checkbox),
     CaseStepModel("3. Get Affidavits Prepared for MSJ Response", .checkbox),
     CaseStepModel("4. MSJ Brief Filed", .checkbox),
     CaseStepModel("5. Our MSJ and Brief Filed", .checkbox),
     CaseStepModel("6. MSJ Hearing Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("7. MSJ Hearing Held and Order Prepared", .checkbox),
     CaseStepModel("8. If Lost, Letter to Client w/30 Day Notice of Appeal and New Contract", .checkbox)],
    
    // Daubert
    [CaseStepModel("1. Daubert Motion Received", .checkbox),
     CaseStepModel("2. Daubert Hearing Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("3. Daubert Motion Objection Brief Prepared", .checkbox),
     CaseStepModel("4. Daubert Brief Filed", .checkbox),
     CaseStepModel("5. Our Daubert Motion Prepared", .checkbox),
     CaseStepModel("6. Daubert Reply Brief from OC Received", .checkbox),
     CaseStepModel("7. Daubert Sur-Reply Brief Prepared", .checkbox),
     CaseStepModel("8. Daubert Sur-Reply Brief Filed", .checkbox),
     CaseStepModel("9. Daubert Hearing Held and Order Prepared", .checkbox)],
    
    // Request Mediation
    [CaseStepModel("1. Mediation Request Prepared", .checkbox),
     CaseStepModel("2. Mediation Request Filed", .checkbox),
     CaseStepModel("3. Hearing Scheduled and Placed on Calendar", .checkbox),
     CaseStepModel("4. Mediator Selected and Contacted", .text),
     CaseStepModel("5. Mediation Date Scheduled and Placed on Calendar", .text),
     CaseStepModel("6. Client Informed of Mediation Date", .checkbox),
     CaseStepModel("7. Mediation Submission Prepared", .checkbox),
     CaseStepModel("8. Mediation Submission Filed", .checkbox),
     CaseStepModel("9. Mediation Held", .checkbox)],
    
    // Exhibits Prep
    [CaseStepModel("1. Medical Exhibits Done", .checkbox),
     CaseStepModel("2. Lost Wages Exhibits Done", .checkbox),
     CaseStepModel("3. Video Depositions Done", .checkbox),
     CaseStepModel("4. Expert Exhibits Done", .checkbox),
     CaseStepModel("5. Prepare for Trial: ___ Enlargements; __ Demonstrative exhibits", .checkbox),
     CaseStepModel("6. Prepare PowerPoint Opening w/Exhibits", .checkbox),
     CaseStepModel("7. Visit Courtroom to Inspect Electronics", .checkbox),
     CaseStepModel("8. Check with Judge/Staff as to Rule for Showing Items to Jury", .checkbox)],
    
    // Witness Trial Prep
    [CaseStepModel("1. Summarize all Depositions", .checkbox),
     CaseStepModel("2. Check Exhibits and Prepare Foundation for Each", .checkbox),
     CaseStepModel("3. Subpoena Lay Witness", .checkbox),
     CaseStepModel("4. Call Lay Witness to Verify/Arrange Appearance", .checkbox),
     CaseStepModel("5. Subpoena Police", .checkbox),
     CaseStepModel("6. Call Police to Verify/Arrange Appearance", .checkbox),
     CaseStepModel("7. Subpoena Medical Witness", .checkbox),
     CaseStepModel("8. Call Medical Witness to Verify/Arrange Appearance", .checkbox),
     CaseStepModel("9. Draft Witness Exam with Exhibit List Included", .checkbox),
     CaseStepModel("10. Draft Medical Exam w/Exhibits", .checkbox),
     CaseStepModel("11. Draft Police Exam w/Exhibits", .checkbox),
     CaseStepModel("12. Draft Plaintiff Exam w/Exhibits", .checkbox),
     CaseStepModel("13. Meet with Plaintiff to Prepare Trial Testimony", .checkbox)],
    
    // Voir Dire
    [CaseStepModel("1. Get Jury Panel Questionnaires", .checkbox),
     CaseStepModel("2. Check Jurors and Research", .checkbox),
     CaseStepModel("3. Prepare Own Jury Panel Questionnaires", .checkbox),
     CaseStepModel("4. File Jury Panel Questionnaires", .checkbox),
     CaseStepModel("5. Prepare Voir Dire Outline", .checkbox)],
    
    // MILs
    [CaseStepModel("1. Motion in Limine (MIL) Prepared", .checkbox),
     CaseStepModel("2. MIL Filed", .checkbox),
     CaseStepModel("3. OC MIL Received", .checkbox),
     CaseStepModel("4. Prepared MIL Objections", .checkbox),
     CaseStepModel("5. Objections MILs Filed", .checkbox)],
    
    // Jury Instructions
    [CaseStepModel("1. Jury Instructions (JI) Prepared", .checkbox),
     CaseStepModel("2. JIs Filed", .checkbox),
     CaseStepModel("3. OC JIs Received", .checkbox),
     CaseStepModel("4. Prepared JIs Objections", .checkbox),
     CaseStepModel("5. Objections JIs filed", .checkbox)],
    
    // Trial Motions
    [CaseStepModel("1. Prepare Motion for Judgment as Matter of Law", .checkbox),
     CaseStepModel("2. Place Motion for Judgment in File", .checkbox),
     CaseStepModel("3. Prepare Brief in Response to Motion for Directed Verdict", .checkbox),
     CaseStepModel("4. Place Brief in Response to MFDV in File", .checkbox)]
]
