//
//  AppManager.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    static let sharedInstance = AppManager()
    
    override init() {
        super.init()
    }
    
    // terms
    func setTerms(agree: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(agree, forKey: KUserDefaultTerms)
        defaults.synchronize()
    }
    func getTerms() -> Bool {
        let defaults = UserDefaults.standard
        let _agree = defaults.bool(forKey: KUserDefaultTerms)
        return _agree;
    }
    
    // email
    func setEmail(email:String) {
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: KUserDefaultEmail)
        defaults.synchronize()
    }
    func getEmail() -> String {
        let defaults = UserDefaults.standard
        var _email = defaults.string(forKey: KUserDefaultEmail)
        _email = _email == nil ? "":_email
        return _email!
    }
    
    // password
    func setPassword(password:String) {
        let defaults = UserDefaults.standard
        defaults.set(password, forKey: KUserDefaultPassword)
        defaults.synchronize()
    }
    func getPassword() -> String {
        let defaults = UserDefaults.standard
        var _password = defaults.string(forKey: KUserDefaultPassword)
        _password = _password == nil ? "":_password
        return _password!
    }
    
    // name
    func setName(name:String) {
        let defaults = UserDefaults.standard
        defaults.set(name, forKey: KUserDefaultName)
        defaults.synchronize()
    }
    func getName() -> String {
        let defaults = UserDefaults.standard
        var _name = defaults.string(forKey: KUserDefaultName)
        _name = _name == nil ? "":_name
        return _name!
    }
    
    // login keep
    func setLoginKeep(login:Bool){
        let defaults = UserDefaults.standard
        defaults.set(login, forKey: KUserDefaultLogin)
        defaults.synchronize()
    }
    func getLoginKeep()->Bool{
        let defaults = UserDefaults.standard
        let _login = defaults.bool(forKey: KUserDefaultLogin)
        return _login
    }
    
    // admin status
    func setAdminStatus(login:Bool){
        let defaults = UserDefaults.standard
        defaults.set(login, forKey: KUserDefaultAdmin)
        defaults.synchronize()
    }
    func getAdminStatus()->Bool{
        let defaults = UserDefaults.standard
        let _adminStatus = defaults.bool(forKey: KUserDefaultAdmin)
        return _adminStatus;
    }
}
