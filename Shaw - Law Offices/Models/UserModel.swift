//
//  UserModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct UserModel {
    let ref: DatabaseReference?
    let uid: String
    let email: String
    let name: String
    var is_admin:Bool
    var deleted: Bool
    var regToken: String
    
    init(uid: String, email: String, name: String) {
        self.ref = nil
        self.uid = uid
        self.email = email
        self.name = name
        self.is_admin = false
        self.deleted = false
        self.regToken = ""
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let uid = value["uid"] as? String,
            let email = value["email"] as? String,
            let name = value["name"] as? String,
            let is_admin = value["is_admin"] as? Bool,
            let deleted = value["deleted"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.uid = uid
        self.email = email
        self.name = name
        self.is_admin = is_admin
        self.deleted = deleted
        
        if let token = value["regToken"] as? String {
            self.regToken = token
        } else {
            self.regToken = ""
        }
    }
    
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "email": email,
            "name": name,
            "is_admin": is_admin,
            "deleted": deleted
        ]
    }
}

