//
//  HistoryModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct HistoryModel {
    let ref: DatabaseReference?
    let percent: Double
    let detail: String
    let createAt: Int64
    var increase: Bool
    
    init(percent: Double, detail: String, createAt: Int64, increase: Bool) {
        self.ref = nil
        self.percent = percent
        self.detail = detail
        self.createAt = createAt
        self.increase = increase
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let percent = value["percent"] as? Double,
            let detail = value["detail"] as? String,
            let createAt = value["createAt"] as? Int64,
            let increase = value["increase"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.percent = percent
        self.detail = detail
        self.createAt = createAt
        self.increase = increase
    }
    
    func toAnyObject() -> Any {
        return [
            "percent": percent,
            "detail": detail,
            "createAt": createAt,
            "increase": increase
        ]
    }
}
