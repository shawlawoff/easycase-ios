//
//  ClientCaseCell.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit

protocol ClientCaseCellDelegate {
    func deleteAccount(_ uid: String)
}

class ClientCaseCell: UITableViewCell {

    var uid: String!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    var delegate: ClientCaseCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteAction(_ sender: Any) {
        
        if (delegate != nil) {
            delegate?.deleteAccount(uid)
        }
    }
}
