//
//  QuestionnaireViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import FirebaseAuth

class QuestionnaireViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtCell: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewInfoByMail: SingleChoiceView!
    @IBOutlet weak var viewInjured: SingleChoiceView!
    @IBOutlet weak var viewAmbulance: SingleChoiceView!
    @IBOutlet weak var viewMedically: SingleChoiceView!
    @IBOutlet weak var viewEr: SingleChoiceView!
    
    @IBOutlet weak var txtHospital: UITextField!
    @IBOutlet weak var viewInvolved: SingleChoiceView!

    @IBOutlet weak var viewWages: SingleChoiceView!
    @IBOutlet weak var viewInsurance: SingleChoiceView!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtInsuranceCity: UITextField!
    
    @IBOutlet weak var viewFault: SingleChoiceView!
    @IBOutlet weak var viewYourFault: SingleChoiceView!
    @IBOutlet weak var viewDamage: SingleChoiceView!
    @IBOutlet weak var viewEstimatedDamage: SingleChoiceView!
    @IBOutlet weak var viewWreck: SingleChoiceView!
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initializeControls() {
        txtBirthday.delegate = self
        txtDate.delegate = self
        txtTime.delegate = self
        
        let birthPickerView = UIDatePicker()
        birthPickerView.datePickerMode = .date
        birthPickerView.addTarget(self, action: #selector(handleBirthPicker(sender:)), for: .valueChanged)
        txtBirthday.inputView = birthPickerView
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        txtDate.inputView = datePickerView
        
        let timePickerView = UIDatePicker()
        timePickerView.datePickerMode = .time
        timePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        txtTime.inputView = timePickerView
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(logoTapped(tapGestureRecognizer:)))
        imgLogo.isUserInteractionEnabled = true
        imgLogo.addGestureRecognizer(tapGestureRecognizer)
        
        let uid = Auth.auth().currentUser!.uid
        KDataRefQuestion.child(uid).observeSingleEvent(of: .value) { (snapshot) in
            if let item = QuestionnaireModel(snapshot: snapshot) {
                self.txtName.text = item.name
                self.txtBirthday.text = item.birthday
                self.txtAddress.text = item.address
                self.txtCity.text = item.city
                self.txtState.text = item.state
                self.txtZip.text = item.zipcode
                self.txtCell.text = item.cell
                self.txtEmail.text = item.email
                
                self.viewInfoByMail.selectedState = self.convertStringToQuestion(item.infoByMail)
                self.viewInjured.selectedState = self.convertStringToQuestion(item.injured)
                self.viewAmbulance.selectedState = self.convertStringToQuestion(item.ambulance)
                self.viewMedically.selectedState = self.convertStringToQuestion(item.medically)
                self.viewEr.selectedState = self.convertStringToQuestion(item.er)
                
                self.txtHospital.text = item.hospital
                
                self.viewWages.selectedState = self.convertStringToQuestion(item.wages)
                self.viewInvolved.selectedState = self.convertStringToQuestion(item.involved)
                self.viewInsurance.selectedState = self.convertStringToQuestion(item.insurance)
                
                self.txtDate.text = item.dateOfIncident
                self.txtTime.text = item.time
                self.txtInsuranceCity.text = item.insuranceCity
                
                self.viewFault.selectedState = self.convertStringToQuestion(item.fault)
                self.viewYourFault.selectedState = self.convertStringToQuestion(item.yourfault)
                self.viewDamage.selectedState = self.convertStringToQuestion(item.damage)
                self.viewEstimatedDamage.selectedState = self.convertStringToQuestion(item.estimatedDamage)
                self.viewWreck.selectedState = self.convertStringToQuestion(item.wreck)
            }
        }
    }
    
    // MARK: - User Interaction
    @objc func logoTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func handleBirthPicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        txtBirthday.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        txtDate.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        txtTime.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        var model = QuestionnaireModel()
        model.name = txtName.text!
        model.birthday = txtBirthday.text!
        model.address = txtAddress.text!
        model.city = txtCity.text!
        model.state = txtState.text!
        model.zipcode = txtZip.text!
        model.cell = txtCell.text!
        model.email = txtEmail.text!
        
        model.infoByMail = convertQuestionToString(viewInfoByMail.selectedState)
        model.injured = convertQuestionToString(viewInjured.selectedState)
        model.ambulance = convertQuestionToString(viewAmbulance.selectedState)
        model.medically = convertQuestionToString(viewMedically.selectedState)
        model.er = convertQuestionToString(viewEr.selectedState)

        model.hospital = txtHospital.text!

        model.wages = convertQuestionToString(viewWages.selectedState)
        model.involved = convertQuestionToString(viewInvolved.selectedState)
        model.insurance = convertQuestionToString(viewInsurance.selectedState)

        model.dateOfIncident = txtDate.text!
        model.time = txtTime.text!
        model.insuranceCity = txtInsuranceCity.text!

        model.fault = convertQuestionToString(viewFault.selectedState)
        model.yourfault = convertQuestionToString(viewYourFault.selectedState)
        model.damage = convertQuestionToString(viewDamage.selectedState)
        model.estimatedDamage = convertQuestionToString(viewEstimatedDamage.selectedState)
        model.wreck = convertQuestionToString(viewWreck.selectedState)
        
        KDataRefQuestion.child((Auth.auth().currentUser?.uid)!).setValue(model.toAnyObject())
        
        self.performSegue(withIdentifier: Segue.goUserMain, sender: self)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
