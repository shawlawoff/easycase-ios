//
//  UpcomingDateModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/10/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct UpcomingDateModel {
    let ref: DatabaseReference?
    let date: String
    let note: String
    
    init(date: String, note: String) {
        self.ref = nil
        self.date = date
        self.note = note
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let date = value["date"] as? String,
            let note = value["note"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.date = date
        self.note = note
    }
    
    func toAnyObject() -> Any {
        return [
            "date": date,
            "note": note
        ]
    }
}
