//
//  QuestionnaireModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 1/26/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct QuestionnaireModel {
    var ref: DatabaseReference?
    var name: String
    var birthday: String
    var address: String
    var city: String
    var state: String
    var zipcode: String
    var cell: String
    var email: String
    var infoByMail: String
    var injured: String
    var ambulance: String
    var medically: String
    var er: String
    var hospital: String
    var wages: String
    var involved: String
    var insurance: String
    var dateOfIncident: String
    var time: String
    var insuranceCity: String
    var fault: String
    var yourfault: String
    var damage: String
    var estimatedDamage: String
    var wreck: String
    
    init() {
        self.ref = nil
        self.name = ""
        self.birthday = ""
        self.address = ""
        self.city = ""
        self.state = ""
        self.zipcode = ""
        self.cell = ""
        self.email = ""
        self.infoByMail = ""
        self.injured = ""
        self.ambulance = ""
        self.medically = ""
        self.er = ""
        self.hospital = ""
        self.wages = ""
        self.involved = ""
        self.insurance = ""
        self.dateOfIncident = ""
        self.time = ""
        self.insuranceCity = ""
        self.fault = ""
        self.yourfault = ""
        self.damage = ""
        self.estimatedDamage = ""
        self.wreck = ""
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let birthday = value["birthday"] as? String,
            let address = value["address"] as? String,
            let city = value["city"] as? String,
            let state = value["state"] as? String,
            let zipcode = value["zipcode"] as? String,
            let cell = value["cell"] as? String,
            let email = value["email"] as? String,
            let infoByMail = value["infoByMail"] as? String,
            let injured = value["injured"] as? String,
            let ambulance = value["ambulance"] as? String,
            let medically = value["medically"] as? String,
            let er = value["er"] as? String,
            let hospital = value["hospital"] as? String,
            let wages = value["wages"] as? String,
            let involved = value["involved"] as? String,
            let insurance = value["insurance"] as? String,
            let dateOfIncident = value["dateOfIncident"] as? String,
            let time = value["time"] as? String,
            let insuranceCity = value["insuranceCity"] as? String,
            let fault = value["fault"] as? String,
            let yourfault = value["yourfault"] as? String,
            let damage = value["damage"] as? String,
            let estimatedDamage = value["estimatedDamage"] as? String,
            let wreck = value["wreck"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.name = name
        self.birthday = birthday
        self.address = address
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.cell = cell
        self.email = email
        self.infoByMail = infoByMail
        self.injured = injured
        self.ambulance = ambulance
        self.medically = medically
        self.er = er
        self.hospital = hospital
        self.wages = wages
        self.involved = involved
        self.insurance = insurance
        self.dateOfIncident = dateOfIncident
        self.time = time
        self.insuranceCity = insuranceCity
        self.fault = fault
        self.yourfault = yourfault
        self.damage = damage
        self.estimatedDamage = estimatedDamage
        self.wreck = wreck
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "birthday": birthday,
            "address": address,
            "city": city,
            "state": state,
            "zipcode": zipcode,
            "cell": cell,
            "email": email,
            "infoByMail": infoByMail,
            "injured": injured,
            "ambulance": ambulance,
            "medically": medically,
            "er": er,
            "hospital": hospital,
            "wages": wages,
            "involved": involved,
            "insurance": insurance,
            "dateOfIncident": dateOfIncident,
            "time": time,
            "insuranceCity": insuranceCity,
            "fault": fault,
            "yourfault": yourfault,
            "damage": damage,
            "estimatedDamage": estimatedDamage,
            "wreck": wreck
        ]
    }
}
