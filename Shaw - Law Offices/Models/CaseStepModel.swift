//
//  CaseStepModel.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/14/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import Foundation

enum CaseStepViewType {
    case checkbox
    case text
    case textdate
    case binary
}

struct CaseStepModel {
    let text: String
    let type: CaseStepViewType
    
    init(_ text: String, _ type: CaseStepViewType) {
        self.text = text
        self.type = type
    }
    
    func toAnyObject() -> Any {
        return [
            "text": text,
            "type": type
        ]
    }
}
