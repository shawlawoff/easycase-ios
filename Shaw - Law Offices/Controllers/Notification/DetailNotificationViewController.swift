//
//  DetailNotificationViewController.swift
//  Shaw - Law Offices
//
//  Created by Topdeveloper on 2/25/19.
//  Copyright © 2019 Topdeveloper. All rights reserved.
//

import UIKit
import FirebaseAuth

class DetailNotificationViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    
    var notification: NotificationModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if !AppManager.sharedInstance.getAdminStatus(),
//            let user = Auth.auth().currentUser {
//            if !notification.readUsers.contains(user.uid) {
//                notification.readUsers.append(user.uid)
//                KDataRefNotify.child(notification.ref!.key!).setValue(notification.toAnyObject())
//            }
//        }
        
        lblMessage.text = notification.message
    }
    

    // MARK: - User Interaction
    @IBAction func okAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
